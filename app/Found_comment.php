<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Found_comment extends Model
{
     protected $fillable = [
		'item_id',
		'user_id',
		'message'
	];
     
    public function user(){
        return $this->belongsTo('App\User');  
    }
    
    public function found_item(){
        
        return $this->belongsTo('App\Found_item');
        
    }
}
