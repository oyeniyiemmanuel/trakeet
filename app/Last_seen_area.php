<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Last_seen_area extends Model
{
    
    protected $fillable = [
        
        'name'
        
    ];
    
    public function found_items(){
        
        return $this->belongsToMany('App\Found_item');
        
    }
    
    public function missing_items(){
        
        return $this->belongsToMany('App\Missing_item');
        
    }

    public function missing_people(){
        
        return $this->belongsToMany('App\Missing_person');
        
    }
}
