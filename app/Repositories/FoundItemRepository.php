<?php

namespace App\Repositories;

use App\Repositories\Interfaces\FoundItemInterface;
use App\Found_item;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Last_seen_area;
use App\Notifications\NewLinkedReport;
use App\User;
use Image;

class FoundItemRepository implements FoundItemInterface
{
    private $item;

    public function __construct(Found_item $item){
        $this->item = $item;
    }

    public function create($input){
        return $this->item->create($input);
    }

    public function update($input){
        return $this->item->update($input);
    }

    public function paginate(){
        return $this->item->latest()->paginate(9);
    }

    public function findBy($id){
        return $this->item->findOrFail($id);
    }

    public function category($category){
        return $this->item->where('item_category', '=', $category)->latest()->paginate(9);
    }

    public function state($state){
        return $this->item->where('last_seen_state', '=', $state)->latest()->paginate(9);
    }

    public function addPhoto($file, Found_item $item){
        list($width, $height, $type, $attr) = getimagesize($file);
        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // append tn and time to photo name for thumbnail
        $tnName = 'tn-' . $name;
        // upload photo 
        $file->move('uploads/found_items/photos', $name );
        // create thumbnail
        Image::make("uploads/found_items/photos/{$name}")->fit(200)->save("uploads/found_items/thumbnails/{$tnName}");
        // save paths in database
        $item->photos()->create([
                                'path' => "uploads/found_items/photos/{$name}"
                                ,'thumbnail_path' => "uploads/found_items/thumbnails/{$tnName}"
                                ]);
    }

	public function syncAreas(Found_item $item, Request $request){
		// get the ids of all area tags
        $areas_in_db = Last_seen_area::pluck('id')->toArray();
        // get the array of areas inputted
        $areas_list = $request->input('last_seen_area_list');
        //create an empty new array
        $areas = [];
        
        foreach( $areas_list as $input_area ){
                // if the inputted area doesnt exist in the db
                if( !in_array( $input_area, $areas_in_db ) ) {
                    // save as a new one
                    $nw = new Last_seen_area;
                    $nw->name = $input_area;
                    $nw->save();
                    $a = $nw->id;
                    //add the newly saved area to the empty 'areas' array
                    array_push( $areas, $a );

                }else{
                    // if the inputted area exits in the db, add to the empty 'areas' array
                    array_push( $areas, $input_area );
                }
            
        }
        // go ahead and sync the areas to the item
        $item->last_seen_areas()->sync($areas);
	}

	public function makeSlug(Request $request, Array $input){
        
        $slug = str_slug( substr($request->item_description, 0, 30) );
        $input['slug'] = $slug;

        $latestSlug = 
            Found_item::whereRaw("slug RLIKE '^{$input['slug']}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
            $pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $input['slug'].='-'.($number + 1);
        }  
        return $input;
    }

    public function sendResponseNotification($old_report, $new_report, $report_type_url, $recipient){

        $title = substr($new_report->item_description, 0, 30);
        $content = 'A recently reported <em>found item</em> in <strong>'.$new_report->last_seen_state.'</strong> was linked to your previously reported <em>missing item</em> <strong> - <br>"'
                    .$old_report->item_description.'"</strong>';
        $short_content = '<em>New report linked to:</em> '.$old_report->item_description;

        $link = env('APP_URL').$report_type_url.'/'.$new_report->slug;

        $user = User::where('email',$recipient)->first();

        if($user != null){
            $user->notify(new NewLinkedReport($title, $content, $short_content, $link));
        }
    }
}