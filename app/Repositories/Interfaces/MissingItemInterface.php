<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Missing_item;

interface MissingItemInterface{
	function create($input);

	function update($input);

	function findBy($id);

	function category($category);

	function state($state);

	function addPhoto($file, Missing_item $item);
	
	function paginate();

	function syncAreas(Missing_item $item, Request $request);

	function makeSlug(Request $request, Array $input);

	function sendResponseNotification($old_report, $new_report, $report_type_url, $recipient);
}