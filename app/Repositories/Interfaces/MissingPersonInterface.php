<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Missing_person;

interface MissingPersonInterface{
	function create($input);

	function update($input);

	function state($state);

	function addPhoto($file, Missing_person $item);
	
	function paginate();

	function syncAreas(Missing_person $item, Request $request);

	function syncFeatures(Missing_person $missing_person, Request $request);

	function syncDisabilities(Missing_person $missing_person, Request $request);

	function syncLanguages(Missing_person $missing_person, Request $request);

	function makeSlug(Request $request, Array $input);

}