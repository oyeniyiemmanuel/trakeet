<?php
namespace App\Repositories;

use App\Repositories\Interfaces\MissingPersonInterface;
use App\Missing_person;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Last_seen_area;
use App\Disability;
use App\Distinct_feature;
use App\Language;
use App\User;
use Image;

class MissingPersonRepository implements MissingPersonInterface
{
    private $person;

    public function __construct(Missing_person $person){
        $this->person = $person;
    }

    public function create($input){
        return $this->person->create($input);
    }

    public function update($input){
        return $this->person->update($input);
    }

    public function paginate(){
        return $this->person->latest()->paginate(9);
    }

    public function state($state){
        return $this->person->where('last_seen_state', '=', $state)->latest()->paginate(9);
    }

    public function addPhoto($file, Missing_person $missing_person){
        list($width, $height, $type, $attr) = getimagesize($file);
        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // append tn and time to photo name for thumbnail
        $tnName = 'tn-' . $name;
        // upload photo 
        $file->move('uploads/missing_people/photos', $name );
        // create thumbnail
        Image::make("uploads/missing_people/photos/{$name}")->fit(200)->save("uploads/missing_people/thumbnails/{$tnName}");
        // save paths in database
        $missing_person->photos()->create([
                                'path' => "uploads/missing_people/photos/{$name}"
                                ,'thumbnail_path' => "uploads/missing_people/thumbnails/{$tnName}"
                                ]);
    }

	public function syncAreas(Missing_person $missing_person, Request $request){
        // get the ids of all area tags
        $areas_in_db = Last_seen_area::pluck('id')->toArray();
        // get the array of areas inputted
        $areas_list = $request->input('last_seen_area_list');
        //create an empty new array
        $areas = [];
        
        foreach( $areas_list as $input_area ){
                // if the inputted area doesnt exist in the db
                if( !in_array( $input_area, $areas_in_db ) ) {
                    // save as a new one
                    $nw = new Last_seen_area;
                    $nw->name = $input_area;
                    $nw->save();
                    $a = $nw->id;
                    //add the newly saved area to the empty 'areas' array
                    array_push( $areas, $a );

                }else{
                    // if the inputted area exits in the db, add to the empty 'areas' array
                    array_push( $areas, $input_area );
                }
            
        }
        // go ahead and sync the areas to the person
        $missing_person->last_seen_areas()->sync($areas);
        
    }

    public function syncFeatures(Missing_person $missing_person, Request $request){
        // get the ids of all area tags
        $in_db = Distinct_feature::pluck('id')->toArray();
        // get the array of areas inputted
        $list = $request->input('distinct_feature_list');
        //create an empty new array
        $array = [];
        
        foreach( $list as $input ){
                // if the inputted area doesnt exist in the db
                if( !in_array( $input, $in_db ) ) {
                    // save as a new one
                    $nw = new Distinct_feature;
                    $nw->name = $input;
                    $nw->save();
                    $a = $nw->id;
                    //add the newly saved area to the empty 'areas' array
                    array_push( $array, $a );

                }else{
                    // if the inputted area exits in the db, add to the empty 'areas' array
                    array_push( $array, $input );
                }
            
        }
        // go ahead and sync the areas to the person
        $missing_person->distinct_features()->sync($array);
        
    }

    public function syncDisabilities(Missing_person $missing_person, Request $request){
        // get the ids of all area tags
        $in_db = Disability::pluck('id')->toArray();
        // get the array of areas inputted
        $list = $request->input('disability_list');
        //create an empty new array
        $array = [];
        
        foreach( $list as $input ){
                // if the inputted area doesnt exist in the db
                if( !in_array( $input, $in_db ) ) {
                    // save as a new one
                    $nw = new Disability;
                    $nw->name = $input;
                    $nw->save();
                    $a = $nw->id;
                    //add the newly saved area to the empty 'areas' array
                    array_push( $array, $a );

                }else{
                    // if the inputted area exits in the db, add to the empty 'areas' array
                    array_push( $array, $input );
                }
            
        }
        // go ahead and sync the areas to the person
        $missing_person->disabilities()->sync($array);
        
    }

    public function syncLanguages(Missing_person $missing_person, Request $request){
        // get the ids of all area tags
        $in_db = Language::pluck('id')->toArray();
        // get the array of areas inputted
        $list = $request->input('language_list');
        //create an empty new array
        $array = [];
        
        foreach( $list as $input ){
                // if the inputted area doesnt exist in the db
                if( !in_array( $input, $in_db ) ) {
                    // save as a new one
                    $nw = new Language;
                    $nw->name = $input;
                    $nw->save();
                    $a = $nw->id;
                    //add the newly saved area to the empty 'areas' array
                    array_push( $array, $a );

                }else{
                    // if the inputted area exits in the db, add to the empty 'areas' array
                    array_push( $array, $input );
                }
            
        }
        // go ahead and sync the areas to the person
        $missing_person->languages()->sync($array);
        
    }

    public function makeSlug(Request $request, Array $input){
        
        $slug = str_slug( substr($request->person_name, 0, 30) );
        $input['slug'] = $slug;

        $latestSlug = 
            Missing_person::whereRaw("slug RLIKE '^{$input['slug']}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
            $pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $input['slug'].='-'.($number + 1);
        }  
        return $input;
    }
}