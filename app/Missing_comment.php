<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Missing_comment extends Model
{
    protected $fillable = [
        'item_id',
		'user_id',
		'message'
	];
    
    public function user(){
        return $this->belongsTo('App\User');  
    }
    
    public function missing_item(){
        
        return $this->belongsTo('App\Missing_item');
        
    }
    
}
