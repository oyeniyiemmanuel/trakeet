<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
	
    protected $fillable = [
    	'name'
    ];

    public function missing_people(){

    	return $this->belongsToMany('App\Missing_person');
    }
}
