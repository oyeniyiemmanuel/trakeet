<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
        'name', 'email', 'trakeet_code_no'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
