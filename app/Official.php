<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Official extends Model
{
    protected $fillable = [
        'name', 'email', 'office_type', 'trakeet_code_no', 'office_code_no'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'email', 'email');
    }
}
