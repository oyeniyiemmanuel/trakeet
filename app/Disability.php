<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disability extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function missing_people(){

    	return $this->belongsToMany('App\Missing_person');
    }
}
