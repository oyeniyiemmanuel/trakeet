<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Tracker;

class Found_item extends Model
{
    use Searchable;

    public function searchableAs(){
        return 'Found_items_index';
    }

    protected $primaryKey = 'id';

    public function toSearchableArray(){
        
        $array = [
            'id' => $this->id
            ,'item_description' => $this->item_description
        ];

        return $array;
    }

    protected $fillable = [
		'slug',
		'item_category',
		'item_description',
        'last_seen_state',
		'last_seen_date',
		'last_seen_time',
		'reporter_email',
		'reporter_phone',
		'reporter_name'
	];
    
    protected $dates = [
        'last_seen_date'
    ];

    public function getLastSeenDateAttribute($dates){

        return Carbon::parse($dates);

    }

    public function user(){
        return $this->belongsTo('App\User');  
    }
     
    public function last_seen_areas(){
        
        return $this->belongsToMany('App\Last_seen_area')->withTimestamps();
        
    }

    public function conversations(){
        
        return $this->belongsToMany('App\Found_comment', 'found_conversations')
            ->withTimestamps()
            ->latest('pivot_updated_at');
        
    }
    
    public function linked(){

        return $this->belongsToMany('App\Missing_item', 'missing_but_found_items')
            ->withTimestamps()
            ->latest('pivot_updated_at');

    }

    public function getLastSeenAreaListAttribute(){
        
        return $this->last_seen_areas()->pluck('id')->all();
        
    }
    
    public function photos(){

        return $this->hasMany('App\Found_photo');

    }

    public function getRouteKeyName(){
        
        return 'slug';
        
    }

    public function views($slug){

         return Tracker::logByRouteName('found_items.show')
                            ->where(function($query) use ($slug)
                            {
                                $query
                                    ->where('parameter', 'found_item')
                                    ->where('value', $slug);
                            })
                            ->select('tracker_log.session_id')
                            ->groupBy('tracker_log.session_id')
                            ->distinct()
                            ->count('tracker_log.session_id');

    }
    
}
