<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_official', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function admin(){
        return $this->hasOne('App\Admin');
    }

    public function official(){
        return $this->hasOne('App\Official', 'email', 'email')->get()->first();
    }

    public function missing_people(){
        
        return $this->hasMany('App\Missing_person')->latest();
    
    }
    
    public function found_items(){
        
        return $this->hasMany('App\Found_item')->latest();
    
    }
    
    public function missing_items(){
        
        return $this->hasMany('App\Missing_item')->latest();
        
    }
    
     
    public function comments(){
        
        return $this->hasMany('App\Comment');
        
    }
    
    public function isAGovernmentOfficial(){

        return true;
        
    }
    
}
