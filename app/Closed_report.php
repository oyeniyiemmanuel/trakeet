<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Closed_report extends Model
{
    protected $fillable = [
    	'report_id',
    	'report_type_url'
    ];
}
