<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MissingPersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return ['person_name'=>'required',
                'gender'=>'required',
                'person_description'=>'required',
                'state_of_origin'=>'required',
                'local_government'=>'required',
                'last_seen_state'=>'required',
                'last_seen_area_list'=>'required',
                'distinct_feature_list'=>'required',
                'disability_list'=>'required',
                'language_list'=>'required',
                'last_seen_date'=>'required',
                'last_seen_time'=>'required',
                'reporter_email'=>'required|email',
                'reporter_name'=>'required',
                'reporter_phone'=>'required'
        ];
    }

    public function messages()
    {
        return ['person_name.required'=>'Enter the name of the missing person',   
                 'gender.required'=>'Select the person\'s gender',   
                 'person_description.required'=>'Give little a description of the person',   
                 'state_of_origin.required'=>'Enter the person\'s state of origin',   
                 'local_government.required'=>'Enter the person\'s local government',   
                 'last_seen_state.required'=>'select last seen state',   
                 'last_seen_area_list.required'=>'enter the area where the person was last seen',   
                 'distinct_feature_list.required'=>'enter some distinct features of the person',   
                 'disability_list.required'=>'enter the person\'s disabilities if any',   
                 'language_list.required'=>'languages spoken by the person',   
                 'last_seen_date.required'=>'last seen date not selected',   
                 'last_seen_time.required'=>'select last seen time of the day',   
                 'reporter_name.required'=>'enter your name',   
                 'reporter_phone.required'=>'your phone number is important',   
                 'reporter_email.required'=>'please enter your email address',   
                 'reporter_email.email'=>'please enter a proper email address' 
        ];
    }
}
