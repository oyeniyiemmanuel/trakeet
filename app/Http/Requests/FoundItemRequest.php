<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FoundItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['item_category'=>'required',
                'item_description'=>'required',
                'last_seen_state'=>'required',
                'last_seen_area_list'=>'required',
                'last_seen_time'=>'required',
                'reporter_name'=>'required',
                'reporter_phone'=>'required',
                'reporter_email'=>'required|email'];
    }

    public function messages()
    {
        return ['item_category.required'=>'you have not selected the type of item',   
                 'item_description.required'=>'please describe what you found',   
                 'last_seen_state.required'=>'select state',   
                 'last_seen_area_list.required'=>'enter the area where you found the item',   
                 'last_seen_time.required'=>'what time of the day did you find it?',   
                 'reporter_name.required'=>'enter your name',   
                 'reporter_phone.required'=>'your phone number is important for the owner to locate you',   
                 'reporter_email.required'=>'please enter your email address',   
                 'reporter_email.email'=>'please enter a proper email address'  ];
    }
}
