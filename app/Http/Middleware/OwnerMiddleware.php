<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class OwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next, $table_name)
    {

        if ($request->user()->email != $request->{$table_name}->reporter_email) {

            flash()->error('Oops!','Unauthorized Action: This item was not posted by you.');

            return redirect()->intended('/'.$table_name);

        }

        return $next($request);
    }
}
