<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Missing_item;
use App\Found_item;
use App\Missing_person;

class MainController extends Controller
{
    public function homepage(){

    	//get data from the missing_items table in the db through App\Missing_items model 
		$m_items = Missing_item::latest()->take(3)->get();
		//get data from the found_items table in the db through App\Found_item model 
		$f_items = Found_item::latest()->take(3)->get();
		//get data from the missing_peopl table in the db through App\Missing_person model 
    	$missing_people = Missing_person::latest()->take(3)->get();
         
		//pass the data into homeV.blade.php view
		return view('plain.homeV', compact('m_items', 'f_items', 'missing_people'));
		
	}

    public function team(){
		
		return view('plain.teamV');
	}

    public function how_it_works(){
		//get data from the missing_items table in the db through App\Missing_items model 
		$m_items = Missing_item::latest()->paginate(9);
		//get data from the found_items table in the db through App\Found_item model 
		$f_items = Found_item::latest()->paginate(9);
		//get data from the missing_peopl table in the db through App\Missing_person model 
    	$missing_people = Missing_person::latest()->paginate(9);
         
		//pass the data into homeV.blade.php view
		return view('plain.how_it_worksV', compact('m_items', 'f_items', 'missing_people'));
	}

	public function terms(){
		return view('plain.termsV');
	}

	public function privacy(){
		return view('plain.privacyV');
	}
}
