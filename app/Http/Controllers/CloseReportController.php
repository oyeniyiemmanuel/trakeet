<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Found_item;
use App\Missing_item;
use App\Missing_person;
use App\Closed_report;

class CloseReportController extends Controller
{
    public function __construct(){
        $this->middleware(['auth']);    

        //flash message
        flash()->success('Closed','Report status changed');
    }

    public function found_items(Found_item $report){
    	$report->closed = true;
    	$report->save();
    	
    	Closed_report::firstOrCreate([
    		'report_id' => $report->id,
    		'report_type_url' => '/found_items'
    		]);

    	return redirect('found_items/'.$report->slug);
    }

    public function missing_items(Missing_item $report){
    	$report->closed = true;
    	$report->save();
    	
    	Closed_report::firstOrCreate([
    		'report_id' => $report->id,
    		'report_type_url' => '/missing_items'
    		]);

    	return redirect('missing_items/'.$report->slug);
    }

    public function Missing_people(Missing_person $report){
    	$report->closed = true;
    	$report->save();
    	
    	Closed_report::firstOrCreate([
    		'report_id' => $report->id,
    		'report_type_url' => '/missing_people'
    		]);

    	return redirect('missing_people/'.$report->slug);
    }

}
