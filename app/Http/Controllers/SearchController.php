<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Found_item;
use App\Missing_item;

class SearchController extends Controller
{
    public function search(Request $request){
    	$query = $request->get('query');
        $report_type = preg_replace('/(^[\"\']|[\"\']$)/', '', $request->get('report_type'));

    	// error message if no results found.
        $error = ['error' => 'No results found, please try with different keywords or change filter.'];
        // Making sure the user entered a keyword.
        if($request->has('query')) {
            //search.
            $reports = $report_type::search($query)->paginate(3);
            // get total
            $total = count( $report_type::search($query)->get() ) ;           
            // If there are results return them, if none, return the error message.
            return $reports->count() ? view('search.resultsV', compact('reports', 'query', 'total', 'report_type')) : view('search.resultsV', compact('error', 'query', 'total', 'report_type'));

        }
        // Return the error message if no keywords existed
        return view('search.resultsV', compact('error', 'query', 'total', 'report_type'));

    }
}
