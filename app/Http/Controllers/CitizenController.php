<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Missing_item;
    
use App\Found_item;

use Auth;

class CitizenController extends Controller
{
    public function get_found_items(){
        // get the logged in user
        $user = User::where('email', Auth::user()->email )->first();
        // get all found items submitted when the user was logged in
        $f_items1 = $user->found_items;
        // get all found items submitted when the user was not logged in
        $f_items2 = Found_item::where([
                                        ['reporter_email', '=', Auth::user()->email],
                                        ['user_id','=', 0]
                                        ])
                                    ->latest()
                                    ->get();   
        
        //pass data to citizen.my_fiV.blade.php
        return view('citizen.my_fiV', compact('f_items1', 'f_items2'));
        
    }
    
    public function get_missing_items(){
        // get the logged in user
        $user = User::where('email', Auth::user()->email )->first();
        // get all found items submitted when the user was logged in
        $m_items1 = $user->missing_items;
        // get all found items submitted when the user was not logged include
        $m_items2 = Missing_item::where([
                                        ['reporter_email', Auth::user()->email],
                                        ['user_id', 0]
                                        ])
                                    ->latest()
                                    ->get();
        // pass data to citizen.my_miV.blade.php
        return view('citizen.my_miV', compact('m_items1', 'm_items2'));
    
    }
}
