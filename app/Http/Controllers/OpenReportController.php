<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Found_item;
use App\Missing_item;
use App\Missing_person;
use App\Closed_report;

class OpenReportController extends Controller
{
     public function __construct(){
        $this->middleware(['auth']);    

        //flash message
        flash()->success('Opened','Report status changed');
    }

    public function found_items(Found_item $report){
    	$report->closed = false;
    	$report->save();
    	
    	Closed_report::where([
    		'report_id' => $report->id,
    		'report_type_url' => '/found_items'
    		])->delete();

    	return redirect('found_items/'.$report->slug);
    }

    public function missing_items(Missing_item $report){
    	$report->closed = false;
    	$report->save();
    	
    	Closed_report::where([
    		'report_id' => $report->id,
    		'report_type_url' => '/missing_items'
    		])->delete();

    	return redirect('missing_items/'.$report->slug);
    }

    public function missing_people(Missing_person $report){
    	$report->closed = false;
    	$report->save();
    	
    	Closed_report::where([
    		'report_id' => $report->id,
    		'report_type_url' => '/missing_people'
    		])->delete();

    	return redirect('missing_people/'.$report->slug);
    }
}
