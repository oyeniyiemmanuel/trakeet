<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\MissingItemRequest;
use App\Missing_item;
use App\Repositories\Interfaces\MissingItemInterface;
use App\Repositories\Interfaces\FoundItemInterface;
use Auth;

class MissingItemsController extends Controller
{
    private $repository;

    private $fiRepository;
    
    public function __construct(MissingItemInterface $repository, FoundItemInterface $fiRepository){

        $this->middleware(['auth', 'owner:missing_items'], ['only' => 'edit']);

        $this->repository = $repository;    

        $this->fiRepository = $fiRepository;
    }
    
    public function index(){
       
		$m_items = $this->repository->paginate();
         
		return view('missing.indexV', compact('m_items'));
    }
    
    public function create(){
               
		return view('missing.createV');

    }
    
    public function show( Missing_item $item ){

		$m_items = $this->repository->paginate();

        $comments = $item->conversations;

		return view('missing.itemfocusV', compact('item', 'm_items', 'comments'));
    }
    
    public function store(MissingItemRequest $request){

		$input = $request->all();

        $input = $this->repository->makeSlug($request, $input);

        if(Auth::guest()){

            $item = $this->repository->create($input);

            $this->repository->syncAreas($item, $request);

            if($request->get('response_action')  && $request->get('response_action')=='claim_it'){

                $f_item = $this->fiRepository->findBy($request->get('item_id'));

                $f_item->linked()->attach($item);

                $this->repository->sendResponseNotification($f_item, $item, '/missing_items', $f_item->reporter_email);
            }
            
        }else{

            $item = Auth::user()->missing_items()->save(new Missing_item($input));

            $this->repository->syncAreas($item, $request);

            if($request->get('response_action')  && $request->get('response_action')=='claim_it'){

                $f_item = $this->fiRepository->findBy($request->get('item_id'));

                $f_item->linked()->attach($item);

                $this->repository->sendResponseNotification($f_item, $item, '/missing_items', $f_item->reporter_email);
            }
            
		}
        
        flash()->success('Success','Missing item reported!');

        return redirect('missing_items');
    }
    
    public function edit(Missing_item $item){

        if( Auth::user()->email != $item->reporter_email ){

             flash()->error('Sorry','This item wasn\'t posted by you!');
            return view('missing.itemfocusV', compact('item'));
        }

        return view('missing.editV', compact('item'));
    }
    
    public function update(MissingItemRequest $request, Missing_item $item){
        
        $this->repository->update($request->all());

        $this->repository->syncAreas($item, $request);

		$m_items = $this->repository->paginate();

        $comments = $item->conversations;

        flash()->success('Success','Item report updated!');

        return view('missing.itemfocusV', compact('item', 'm_items', 'comments'));
    }

    public function destroy($id){
        // delete photo uploads related to the item
        foreach ($id->photos as $photo) {
            $photo->destroy($photo->id);
         } 
        // delete item from db
        $id->delete();

        flash()->success('Report Deleted','Your missing item report was successfully removed!');
        
        return 'deleted';
    }

    public function categories($category){

        $m_items = $this->repository->category($category);

        return view('missing.indexV', compact('m_items'));

    }

    public function states($state){

        $m_items = $this->repository->state($state);

        return view('missing.indexV', compact('m_items'));

    }
   
    public function addPhoto(Request $request, Missing_item $item){

        $this->validate($request, ['photo' => 'required|mimes:jpg,jpeg,png,bmp']);

        $file = $request->file('photo');

        $this->repository->addPhoto($file, $item);
    }

    public function found_it(Missing_item $item){

        return view('missing.found_itV', compact('item'));

    }

}












