<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\NewCommentOnReport;
use App\Http\Requests;

use App\User;
use App\Missing_item;
use App\Found_item;
use App\Found_comment;
use App\Missing_comment;
use App\Found_conversation;
use App\Missing_conversation;
use App\Missing_person;
use App\Person_comment;
use Auth;
use Mail;

class CommentsController extends Controller
{
    public function __construct(){
        
        $this->middleware('auth');
        
    }
    
    public function store(Request $request){
        
        if($request->get('item_type') == '/found_items'){

            $save = Found_comment::create($request->all());

            $f_item = Found_item::findOrFail($request->get('item_id'));

            $f_item->conversations()->attach($save);

            $new_comment = $save;
            
            $item_type_url = '/found_items';

            $view = view('partials.new_comment', compact('new_comment', 'item_type_url'));

            $this->sendNotificationMail( $f_item, 'found item', $item_type_url, $request->get('user_id'));

            return response($view)->getOriginalContent(); 

            
        
        }
        
        if($request->get('item_type') == '/missing_items'){
        
            $save = Missing_comment::create($request->all());

            $m_item = Missing_item::findOrFail($request->get('item_id'));

            $m_item->conversations()->attach($save);

            $new_comment = $save;

            $item_type_url = '/missing_items';

            $view = view('partials.new_comment', compact('new_comment', 'item_type_url'));

            $this->sendNotificationMail( $m_item, 'missing item', $item_type_url, $request->get('user_id'));

            return response($view)->getOriginalContent(); 

        }
        
        if($request->get('item_type') == '/missing_people'){
        
            $save = Person_comment::create($request->all());

            $person = Missing_person::findOrFail($request->get('item_id'));

            $person->conversations()->attach($save);

            $new_comment = $save;

            $item_type_url = '/missing_people';

            $view = view('partials.new_comment', compact('new_comment', 'item_type_url'));

            $this->sendNotificationMail( $person, 'missing person', $item_type_url, $request->get('user_id'));

            return response($view)->getOriginalContent(); 
        
        }
        
    }

    public function delete(Request $request, $id){

        if($request->get('item_type') == '/found_items'){

            Found_comment::find($id)->delete();

            $view = view('partials.comment_deleted');

            return response($view)->getOriginalContent(); 

        }

        if($request->get('item_type') == '/missing_items'){
            
            Missing_comment::find($id)->delete();

            $view = view('partials.comment_deleted');

            return response($view)->getOriginalContent(); 

        }

        if($request->get('item_type') == '/missing_people'){
            
            Person_comment::find($id)->delete();

            $view = view('partials.comment_deleted');

            return response($view)->getOriginalContent(); 

        }

    }

    public function sendNotificationMail($report, $report_type, $report_type_url, $user_id){

       if($report_type == 'missing person'){
            $title = substr($report->person_name, 0, 30);
            $content = 'you have a new comment on your <em>'.$report_type.'</em> report - <br> <strong>'.$report->person_name.'</strong>';
            $short_content = '<em>New comment on:</em> '.$report->person_name;
       }else{
            $title = substr($report->item_description, 0, 30);
            $content = 'you have a new comment on your <em>'.$report_type.'</em> report - <br> <strong>'.$report->item_description.'</strong>';
            $short_content = '<em>New comment on:</em> '.$report->item_description;
       }

       $recipient = User::findOrFail($user_id);
       $link = env('APP_URL').$report_type_url.'/'.$report->slug;

       $recipient->notify(new NewCommentOnReport($title, $content, $short_content, $link));
    }
    
}
