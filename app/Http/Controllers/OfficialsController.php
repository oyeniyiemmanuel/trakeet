<?php

namespace App\Http\Controllers;

use App\User;
use App\Official;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;

class OfficialsController extends Controller
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest');
    }


    public function input_id(){

    	return view('officials.input_idV');
    }

    public function confirm_id(Request $request){
    	/*NOTE TO PROGRAMMER: remember to later run a validation against the official ID */

    	$official_code_no = $request->get('official_code_no');

    	flash()->success('Code Accepted','Proceed to filling your details');

    	return view('officials.createV', compact('official_code_no'));
    }

    public function create(Request $request)
    {

         User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'is_official' => true,
        ]);

         Official::create([
            'name' => $request->get('name'),
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'office_type' => 'police',
            'trakeet_code_no' => 'TR-001-0005',
            'office_code_no' => $request->get('official_code_no'),
        ]);

         flash()->success('Registration successful','please login with your details');

         return redirect('/login');

    }
}
