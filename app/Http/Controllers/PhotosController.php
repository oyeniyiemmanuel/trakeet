<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Found_photo;
use App\Missing_photo;
use App\Person_photo;

class PhotosController extends Controller
{
    public function __construct(){

    	$this->middleware(['auth']);

    }

    public function destroy_found_photo($id){

    	$photo = Found_photo::findOrFail($id);

    	$photo->destroy($id);

    	return 'deleted';

    }

    public function destroy_missing_photo($id){

        $photo = Missing_photo::findOrFail($id);

        $photo->destroy($id);

        return 'deleted';

    }

    public function destroy_missing_person_photo($id){

    	$photo = Person_photo::findOrFail($id);

    	$photo->destroy($id);

    	return 'deleted';

    }
}
