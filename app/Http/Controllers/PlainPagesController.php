<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PlainPagesController extends Controller
{
    public function testimonies(){

    	return view('plain.testimonies');
    }

    public function statistics(){

    	return view('plain.statistics');
    }


    public function faq(){

    	return view('plain.faq');
    }

    public function Disclaimer(){

    	return view('plain.disclaimer');
    }
}
