<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class NotificationsController extends Controller
{
    public function markAsRead(User $user){
    	//dd($user);

    	$user->notifications->map(function ($n){
    		$n->markAsRead();
    	});
    }
}
