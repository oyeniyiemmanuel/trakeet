<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MissingPersonRequest;
use App\Repositories\Interfaces\MissingPersonInterface;
use App\Missing_person;
use Auth;


class MissingPeopleController extends Controller
{
    private $repository;

    public function __construct(MissingPersonInterface $repository){

    	$this->middleware(['auth'], ['only' => ['edit', 'addPhoto']]);
        $this->repository = $repository; 
    	
    }

    public function index(){

    	$missing_people = $this->repository->paginate();

    	return view('people.indexV', compact('missing_people'));
    	
    }

    public function show( Missing_person $missing_person ){

        $comments = $missing_person->conversations;

    	return view('people.personFocusV', compact('missing_person', 'comments'));
    	
    }

    public function create(){

    	return view('people.createV');
    	
    }

    public function store(MissingPersonRequest $request){

    	$input = $request->all();

        $input =$this->repository->makeSlug($request, $input); 

        if(Auth::guest()){

            $missing_person = $this->repository->create($input);
            //sync last_seen_area_list, distinct_features, disabilities and languages
            $this->repository->syncAreas($missing_person, $request);
            $this->repository->syncFeatures($missing_person, $request);
            $this->repository->syncDisabilities($missing_person, $request);
            $this->repository->syncLanguages($missing_person, $request);
            
        }else{

            $missing_person = Auth::user()->missing_people()->save(new Missing_person($input));
            //sync last_seen_area_list, distinct_features, disabilities and languages
            $this->repository->syncAreas($missing_person, $request);
            $this->repository->syncFeatures($missing_person, $request);
            $this->repository->syncDisabilities($missing_person, $request);
            $this->repository->syncLanguages($missing_person, $request);
            
		}
        
        flash()->success('Success','Missing person reported!');

		return redirect('missing_people');
    }

    public function edit(Missing_person $missing_person){

        if( Auth::user() && Auth::user()->email != $missing_person->reporter_email ){

            flash()->error('Sorry','This report wasn\'t posted by you!');
            
            return view('people.personFocusV', compact('missing_person'));
        }

        return view('people.editV', compact('missing_person'));

    }

    public function update(MissingPersonRequest $request, Missing_person $missing_person){

        $missing_person->update($request->all());
        //sync last_seen_area_list, distinct features, disabilities and languages
        $this->repository->syncAreas($missing_person, $request);
        $this->repository->syncFeatures($missing_person, $request);
        $this->repository->syncDisabilities($missing_person, $request);
        $this->repository->syncLanguages($missing_person, $request);
        
        $comments = $missing_person->conversations;

        flash()->success('Success','Missing person report updated!');
        
        return view('people.personFocusV', compact('missing_person', 'comments'));
    }

    public function destroy($id){
        // delete photo uploads related to the item
        foreach ($id->photos as $photo) {
            $photo->destroy($photo->id);
         } 
        // delete item from db
        $id->delete();

        flash()->success('Report Deleted','Your missing person report was successfully removed!');

        return 'deleted';
    }

    public function states($state){

        $missing_people = $this->repository->state($state);

        return view('people.indexV', compact('missing_people'));

    }

    public function addPhoto(Request $request, Missing_person $missing_person){

        $this->validate($request, ['photo' => 'required|mimes:jpg,jpeg,png,bmp']);

        $file = $request->file('photo');

        $this->repository->addPhoto($file, $missing_person);

    }

}
