<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\FoundItemRequest;
use App\Found_item;
use App\Repositories\Interfaces\FoundItemInterface;
use App\Repositories\Interfaces\MissingItemInterface;
use Auth;

class FoundItemsController extends Controller
{
    private $repository;

    private $miRepository;

    public function __construct(FoundItemInterface $repository, MissingItemInterface $miRepository){

        $this->middleware(['auth', 'owner:found_items'], ['only' => ['edit', 'addPhoto']]);    

        $this->repository = $repository;

        $this->miRepository = $miRepository;
    }
    
    public function index(){

		$f_items = $this->repository->paginate();

		return view('found.indexV', compact('f_items'));
	}
    
    public function create(){
        
        return view('found.createV');
    }
    
    public function show( Found_item $item ){

		$f_items = $this->repository->paginate();

        $comments = $item->conversations;
        
        return view('found.itemfocusV', compact('item', 'f_items', 'comments'));
	
    }
    
    public function store(FoundItemRequest $request){

		$input = $request->all();

        $input = $this->repository->makeSlug($request, $input);     

        if(Auth::guest()){

            $item = $this->repository->create($input);

            $this->repository->syncAreas($item, $request);

            if($request->get('response_action')  && $request->get('response_action')=='found_it'){

                $m_item = $this->miRepository->findBy($request->get('item_id'));

                $m_item->linked()->attach($item);

                $this->repository->sendResponseNotification($m_item, $item, '/found_items', $m_item->reporter_email);
            }
            
        }else{

            $item = Auth::user()->found_items()->save(new Found_item($input));

            $this->repository->syncAreas($item, $request);

            if($request->get('response_action')  && $request->get('response_action')=='found_it'){

                $m_item = $this->miRepository->findBy($request->get('item_id'));

                $m_item->linked()->attach($item);

                $this->repository->sendResponseNotification($m_item, $item, '/found_items', $m_item->reporter_email);
            }
            
		}

        flash()->success('Success','Found item reported!');

		return redirect('found_items');
	}
    
    public function edit(Found_item $item){

        if( Auth::user()->email != $item->reporter_email ){

            flash()->error('Sorry','This item wasn\'t posted by you!');
            
            return view('found.itemfocusV', compact('item'));
        }

        return view('found.editV', compact('item'));
    }
    
    public function update(FoundItemRequest $request, Found_item $item){

        $this->repository->update($request->all());

        $this->repository->syncAreas($item, $request);

		$f_items = $this->repository->paginate();

        $comments = $item->conversations;

        flash()->success('Success','Item report updated!');

        return view('found.itemfocusV', compact('item', 'f_items', 'comments'));
    }
    
    public function destroy($id){
        // delete photo uploads related to the item
        foreach ($id->photos as $photo) {
            $photo->destroy($photo->id);
         } 
        // delete item from db
        $id->delete();

        flash()->success('Report Deleted','Your found item report was successfully removed!');
        
        return 'deleted';
    }

    public function categories($category){

        $f_items = $this->repository->category($category);

        return view('found.indexV', compact('f_items'));

    }

    public function states($state){

        $f_items = $this->repository->state($state);

        return view('found.indexV', compact('f_items'));

    }

    public function addPhoto(Request $request, Found_item $item){

        $this->validate($request, ['photo' => 'required|mimes:jpg,jpeg,png,bmp']);

        $file = $request->file('photo');
        
        $this->repository->addPhoto($file, $item);

    }

    public function claim_it(Found_item $item){

        return view('found.claim_itV', compact('item'));

    }

    
    
}












