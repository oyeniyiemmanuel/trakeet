<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distinct_feature extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function missing_people(){

    	return $this->belongsToMany('App\Missing_person');
    }
}
