<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Found_photo extends Model
{

	protected $table = 'found_item_photos';

	protected $fillable = [

		'path',
		'thumbnail_path'

	];

    public function found_item(){

    	return $this->belongsTo('App\Found_item');

    }

    public function delete(){

    	\File::delete([

    			$this->path,
    			$this->thumbnail_path

    		]);

    	parent::delete();

    }

}
