<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewCommentOnReport extends Notification
{
    use Queueable;

    protected $title;
    protected $content;
    protected $short_content;
    protected $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($title, $content, $short_content, $link)
    {
        $this->title = $title;
        $this->content = $content;
        $this->short_content = $short_content;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New Comment On: '.$this->title)
                    ->line($this->content)
                    ->action('Click here to go to the report', $this->link);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->title,
            'content' => $this->content,
            'short_content' => $this->short_content,
            'link' => $this->link
        ];
    }
}
