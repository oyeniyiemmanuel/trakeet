<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person_photo extends Model
{
    protected $table = 'missing_people_photo';

	protected $fillable = [

		'path',
		'thumbnail_path'

	];

    public function missing_person(){

    	return $this->belongsTo('App\Missing_person');

    }

    public function delete(){

    	\File::delete([

    			$this->path,
    			$this->thumbnail_path

    		]);

    	parent::delete();

    }
}
