<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Tracker;

class Missing_person extends Model
{
    use Searchable;

    public function searchableAs(){
        return 'Missing_people_index';
    }

    protected $primaryKey = 'id';

    public function toSearchableArray(){
        
        $array = [
            'id' => $this->id
            ,'person_name' => $this->person_name
            ,'person_nickname' => $this->person_nickname
            ,'person_description' => $this->person_description

        ];

        return $array;
    }

    protected $fillable = [
    	'slug'
    	,'person_name'
    	,'person_nickname'
    	,'gender'
    	,'person_description'
    	,'state_of_origin'
    	,'local_government'
    	,'last_seen_state'
    	,'last_seen_date'
    	,'last_seen_time'
    	,'reporter_email'
    	,'reporter_name'
    	,'reporter_phone'
    ];

    protected $dates = [
        'last_seen_date'
    ];

    public function getLastSeenDateAttribute($dates){

        return Carbon::parse($dates);

    }

    public function getRouteKeyName(){
        
        return 'slug';
        
    }

    public function user(){

        return $this->belongsTo('App\User');  
        
    }

    public function last_seen_areas(){
        
        return $this->belongsToMany('App\Last_seen_area')->withTimestamps();
        
    }

    public function conversations(){
        
        return $this->belongsToMany('App\Person_comment', 'missing_person_conversations')
            ->withTimestamps()
            ->latest('pivot_updated_at');
        
    }

     public function photos(){

        return $this->hasMany('App\Person_photo');

    }

    public function distinct_features(){
        
        return $this->belongsToMany('App\Distinct_feature')->withTimestamps();
        
    }

    public function disabilities(){
        
        return $this->belongsToMany('App\Disability')->withTimestamps();
        
    }

    public function languages(){
        
        return $this->belongsToMany('App\Language')->withTimestamps();
        
    }

    public function getLastSeenAreaListAttribute(){
        
        return $this->last_seen_areas()->pluck('id')->all();
        
    }

    public function getDisabilityListAttribute(){
        
        return $this->disabilities()->pluck('id')->all();
        
    }

    public function getDistinctFeatureListAttribute(){
        
        return $this->distinct_features()->pluck('id')->all();
        
    }

    public function getLanguageListAttribute(){
        
        return $this->languages()->pluck('id')->all();
        
    }

    public function views($slug){

         return Tracker::logByRouteName('missing_people.show')
                            ->where(function($query) use ($slug)
                            {
                                $query
                                    ->where('parameter', 'missing_person')
                                    ->where('value', $slug);
                            })
                            ->select('tracker_log.session_id')
                            ->groupBy('tracker_log.session_id')
                            ->distinct()
                            ->count('tracker_log.session_id');

    }
}
