<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Missing_photo extends Model
{
    protected $table = 'missing_item_photos';

	protected $fillable = [

		'path'
		,'thumbnail_path'

	];

    public function missing_item(){

    	return $this->belongsTo('App\Missing_item');

    }

    public function delete(){

    	\File::delete([

    			$this->path,
    			$this->thumbnail_path

    		]);

    	parent::delete();

    }

}
