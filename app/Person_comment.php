<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person_comment extends Model
{

	protected $table = 'person_comments';

    protected $fillable = [
        'item_id',
		'user_id',
		'message'
	];
    
    public function user(){
        return $this->belongsTo('App\User');  
    }
    
    public function missing_person(){
        
        return $this->belongsTo('App\Missing_person');
        
    }
}
