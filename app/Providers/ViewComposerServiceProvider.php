<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Disability;
use App\Distinct_feature;
use App\Language;
use App\Last_seen_area;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
       //initialize founders variables
		$f = ["Gamekyd", "Nakel", "Nuel"];
		$o = [];
		

        //pass the above data into all views
		view()->share(['trakeet_founders'=>$f
						,'others'=>$o
		]);
        
        // Get Nigerian states and pass it to these form views
        view()->composer([
                        'partials.form'
                        ,'partials.dashmenu'
                        ,'partials.missing_person_form'
                        ,'partials.response_form'
                        ], function($view){

            //get last_seen_areas from the db
            $last_seen_areas = Last_seen_area::pluck('name', 'id');
            //get distinct_features from the db
            $distinct_features = Distinct_feature::pluck('name', 'id');
            //get distinct_features from the db
            $languages = Language::pluck('name', 'id');
            //get distinct_features from the db
            $disabilities = Disability::pluck('name', 'id');

             //get the list of all the Nigerian states
            $json_file = file_get_contents("js/states-list.js");
            $states = json_decode($json_file, true);
            asort($states);

            
            $view->with([
                    'states' => $states
                    ,'last_seen_areas' => $last_seen_areas
                    ,'distinct_features' => $distinct_features
                    ,'disabilities' => $disabilities
                    ,'languages' => $languages
                    ]);
            
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
