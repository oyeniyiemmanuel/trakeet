<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\FoundItemInterface;
use App\Repositories\Interfaces\MissingItemInterface;
use App\Repositories\Interfaces\MissingPersonInterface;
use App\Repositories\FoundItemRepository;
use App\Repositories\MissingItemRepository;
use App\Repositories\MissingPersonRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //$this->app->bind('ItemInterface', 'FoundItemRepository');
        $this->app->bind(FoundItemInterface::class, FoundItemRepository::class);
        $this->app->bind(MissingItemInterface::class, MissingItemRepository::class);
        $this->app->bind(MissingPersonInterface::class, MissingPersonRepository::class);
    }
}
