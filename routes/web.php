<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Main 
Route::get('/', 'MainController@homepage');
Route::get('/team', 'MainController@team');
Route::get('/guide', 'MainController@how_it_works');
Route::get('/terms', 'MainController@terms');
Route::get('/privacy', 'MainController@privacy');

// Found Items
Route::resource('found_items', 'FoundItemsController');
Route::get('found_items/categories/{category}', 'FoundItemsController@categories');
Route::get('found_items/states/{state}', 'FoundItemsController@states');
Route::post('found_items/{found_items}/photos', 'FoundItemsController@addPhoto');
Route::get('found_items/{found_items}/claim_it', 'FoundItemsController@claim_it');
Route::get('found_items/{found_items}/close_report', 'CloseReportController@found_items');
Route::get('found_items/{found_items}/open_report', 'OpenReportController@found_items');
Route::delete('found_items_photo/{id}', 'PhotosController@destroy_found_photo');

// Missing items
Route::resource('missing_items', 'MissingItemsController');
Route::get('missing_items/categories/{category}', 'MissingItemsController@categories');
Route::get('missing_items/states/{state}', 'MissingItemsController@states');
Route::post('missing_items/{missing_items}/photos', 'MissingItemsController@addPhoto');
Route::get('missing_items/{missing_items}/found_it', 'MissingItemsController@found_it');
Route::get('missing_items/{missing_items}/close_report', 'CloseReportController@missing_items');
Route::get('missing_items/{missing_items}/open_report', 'OpenReportController@missing_items');
Route::delete('missing_items_photo/{id}', 'PhotosController@destroy_missing_photo');

// Missing People
Route::resource('missing_people', 'MissingPeopleController');
Route::get('missing_people/states/{state}', 'MissingPeopleController@states');
Route::post('missing_people/{missing_people}/photos', 'MissingPeopleController@addPhoto');
Route::get('missing_people/{missing_people}/close_report', 'CloseReportController@missing_people');
Route::get('missing_people/{missing_people}/open_report', 'OpenReportController@missing_people');
Route::delete('missing_people_photo/{id}', 'PhotosController@destroy_missing_person_photo');

// Comments
Route::resource('comments', 'CommentsController');
Route::delete('/commentsdelete/{id}', 'CommentsController@delete');

// Only Logged in Users
Route::group(['middleware' => 'auth'], function(){
    Route::get('users/found_items', 'CitizenController@get_found_items');
    Route::get('users/missing_items', 'CitizenController@get_missing_items');
});

// Only Government Officials
Route::group(['middleware' => 'governmentOfficial'], function(){
    Route::get('officials/found_items/create', 'OfficialFoundItemsController@create');
});

// Plain Pages
Route::get('/testimonies', 'PlainPagesController@testimonies');
Route::get ('/statistics', 'PlainPagesController@statistics');
Route::get ('/faq', 'PlainPagesController@faq');
Route::get ('/disclaimer', 'PlainPagesController@disclaimer');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/home', 'MainController@homepage');

// notifications
Route::delete('users/{user}/notifications', 'NotificationsController@markAsRead');

//search
Route::get('/search', 'SearchController@search');

// Officials
Route::get('/official_id', 'OfficialsController@input_id');
Route::post('/official_id', 'OfficialsController@confirm_id');
Route::post('/register_official', 'OfficialsController@create');
