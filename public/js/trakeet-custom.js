jQuery(document).ready(function(){ 
    //close flash message except the ones with alert-important class
    $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    
    // select2 
    $('#last_seen_area_list').select2({
        placeholder:'e.g. Ikeja',
        tags: true
    });
    $('#distinct_features').select2({
        placeholder:'e.g. Facial Birthmark',
        tags: true
    });
    $('#disabilities').select2({
        placeholder:'e.g. none, blind, mute',
        tags: true
    });
    $('#languages_spoken').select2({
        placeholder:'e.g. french, english, pidgin, yoruba',
        tags: true
    });
    
    $('#last_seen_area_list').on('keyup', function(){
        $('#last_seen_area_list').val(($('#last_seen_area_list').val()).toUpperCase());
    });
    
    
    // submit comments
    $('#talks').on('submit', '#createComment', function(){
        // Getting the link id
        var  id = $(this).attr('id');
        var message = $('#message').val();
        var user_id = $('#user_id').val();
        var item_id = $('#item_id').val();
        var item_type = $('#item_type').val();
        var  href = '/comments';
        var _token = $("input[name='_token']").val();
        //preloader
        $('#showComments').prepend('<div id="new_comment_preloader" style="text-align: center;" class="col-md-12" ><div class="form-group comment_li"><strong>Updating comment...</strong></div></div>');

        $.ajax({
            type: "POST",
            data: {
                message: message
                ,user_id: user_id
                ,item_id: item_id
                ,item_type: item_type
                ,_token: _token
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {	
                //Inserting html into the displayPane div
                $('#showComments').prepend(data);
                $('#new_comment_preloader').detach();
                $('#message').val("");
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                $('#showComments').prepend(error);           
            }
        });
        return false;
    });

    // delete comments
    $('#talks').on('click', '.comment_delete', function(){
        // Getting the link id
        var  id = $(this).attr('id');
        var comment_thread = $(this).parent();
        var comment_id = $(this).parent().find('#comment_id').val();
        var item_type = $(this).parent().find('#item_type').val();
        var  href = '/commentsdelete/' + comment_id;
        var _token = $(this).data("token");
        //preloader
        $(comment_thread).html('<strong style="text-align: center;">deleting comment...</strong>');

        $.ajax({
            type: "DELETE",
            data: {
                comment_id: comment_id
                ,item_type: item_type
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  

                //Inserting html into the displayPane div
                $(comment_thread).html(data);
                $(comment_thread).slideUp(100);
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                $(comment_thread).prepend(error);           
            }
        });
        return false;
    });

    // delete item images
    $('#item_images').on('click', '.delete_button', function(){
        // Getting the link id
        var  id = $(this).parent().attr('id');
        var photo = $(this).parent().parent();
        var photo_id = $(this).parent().find('#photo_id').val();
        var item_type = $(this).parent().find('#item_type').val();
        var  href = item_type + '_photo/' + photo_id;
        var _token = $("input[name='_token']").val();
        //console.log(photo_id);
        //preloader
        $(this).html('<strong style="text-align: center; color:white;">deleting...</strong>');

        $.ajax({
            type: "DELETE",
            data: {
                photo_id: photo_id
                ,item_type: item_type
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  
                //console.log(data);
                $(photo).find('.delete_button').html(data);
                $(photo).slideUp(100);
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });
        return false;
    });

    // mark notifications as read
    $('#notifications').on('click', '.mark_as_read', function(){
        // Getting the link id
        var  user_id = $(this).attr('id');
        var  href = '/users/' + user_id + '/notifications';
        var _token = $(this).data("token");
        $.ajax({
            type: "DELETE",
            data: {
                user_id: user_id
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  
                // clear new notification 
                $('#'+user_id).find('.count').detach();
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);          
            }
        });
        return false;
    });
    // mark notifications as read
    $('#delete_report').on('click', '#delete_report_button', function(){
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              showCancelButton: true,
              confirmButtonColor: '#4D4F6C',
              cancelButtonColor: 'grey',
              confirmButtonText: 'Yes, delete it',
              showLoaderOnConfirm: true,
              closeOnConfirm: false
            },function () {
                var report_slug = $('#delete_report_button').parent().find('#report_slug').val();
                var report_type = $('#delete_report_button').parent().find('#report_type').val();
                var _token = $('#delete_report_button').parent().find("input[name='_token']").val();
                var href = report_type + '/' + report_slug;
                console.log(report_slug);

                $.ajax({
                type: "DELETE",
                data: {
                    report_slug: report_slug
                    ,report_type: report_type
                    ,_token: _token
                    ,_method: 'delete'
                },
                url: href,
                dataType: 'html',
                cache: false,
                success: function (data) {
                    // Redirect to report_type  
                    window.location = report_type;
                },
                error: function(jqXHR, text, error){
                    // Displaying if there are any errors
                    console.log(error);           
                }
            });
            });
        }
    );

    // initialize photoswipe
    $('#item_images').photoSwipe();

    // initialize tooltip
    $('[data-toggle="tooltip"]').tooltip(); 

});

// initialize dropzone
Dropzone.options.addPhotosForm = {
    paramName: 'photo',
    maxFilesize: 2,
    maxFiles: 2,
    acceptedFiles: '.jpg, ,.jpeg, .png, .gif, .bmp',
    init: function() {
        this.on("queuecomplete", function(file) {
            location.reload();
        });
    }
};

// initialize owl carousel
 $(".report_thread_pics").owlCarousel({
      autoPlay: 5000, //Set AutoPlay 
      items: 1,
      navigation: false,
      slideSpeed : 1000,
      paginationSpeed : 4000,
      //transitionStyle : "fadeUp",
      //navigationText: ["<i class='fa fa-angle-double-left'></i>","<i class='fa fa-angle-double-right'></i>"],
      pagination: false,
      itemsDesktop : [1000,1], // items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], // item between 600 and 0
  });

// open social sharing in a new window
    var popupSize = {
        width: 780,
        height: 550
    };

    $(document).on('click', '.social-buttons a', function(e){

        var
            verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        var popup = window.open($(this).prop('href'), 'social',
            'width='+popupSize.width+',height='+popupSize.height+
            ',left='+verticalPos+',top='+horisontalPos+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    });

// general page preloader
$(window).on('load', function() { // makes sure the whole site is loaded
    $('#loader').fadeOut(); // will first fade out the loading animation
    $('#loader-wrapper').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('.content-wrapper').delay(350).css({'overflow-y':'visible'});
});
