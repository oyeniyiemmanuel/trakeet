const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {

    mix.styles([
    		'css/reset.css'
    		,'css/nav-style.css'
    		,'css/font-awesome.min.css'
    		,'bootstrap/css/bootstrap.min.css'
    		,'css/select2.min.css'
    		,'css/sweetalert.css'
    		,'css/photoswipe.css'
    		,'css/photoswipe-default-skin.css'
    		,'css/dropzone.css'
    		,'css/owl.carousel.css'
    		,'css/owl.theme.css'
    		,'css/owl.transitions.css'
    		,'css/trakeet-style.css'
    	], 'public/final/styles.css', 'public')
    	.version('public/final/styles.css');

    mix.scripts([
    		'/js/jquery-3.1.1.min.js'
    		,'/bootstrap/js/bootstrap.min.js'
    		,'/js/select2.full.min.js'
    		,'/js/modernizr.js'
    		,'/js/jquery.menu-aim.js'
    		,'/js/main.js'
    		,'/js/sweetalert-dev.js'
    		,'/js/dropzone.js'
    		,'/js/jquery.photoswipe-global.js'
    		,'/js/owl.carousel.js'
    		,'/js/trakeet-custom.js'
    	], 'public/final/scripts.js', 'public');

    
});
