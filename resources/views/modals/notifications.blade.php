
<!-- show all notifications modal -->
@if(Auth::user())
<div id="all_notifications" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">x</button>
        <h3 class="modal-title">All Notifications</h3>
      </div>

      <div class="modal-footer">
      	<ul class="list-group">
	        @foreach(Auth::user()->notifications as $notification )
		        <a class="list-group-item" href="{{ $notification->data['link'] }}">
					@if( $notification->type == 'App\Notifications\NewLinkedReport' )
					<i class="fa fa-link"></i>&nbsp; 
					@elseif( $notification->type == 'App\Notifications\NewCommentOnReport' )
					<i class="fa fa-comment-o"></i>&nbsp; 
					@else
					<i class="fa fa-bell-o"></i>&nbsp; 
					@endif

		        	<?php echo $notification->data['content'] ?>
		        	<span class="created_at">{{ $notification->created_at->diffForHumans() }}</span>
		        </a>
	        @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
@endif