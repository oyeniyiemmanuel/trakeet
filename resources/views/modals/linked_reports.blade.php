
<div id="report_{{ $report->id }}" class="linked_report_modal modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">x</button>
        <h3 class="modal-title"><strong><em>linked reports to:</em></strong> {{ $report->item_description }}</h3>
      </div>

      <div class="modal-footer">
      @if($report->linked && !$report->linked->isEmpty())
        @foreach($report->linked as $linked_report )
        	<div class="col-md-6 col-sm-6 report_cover">
				<a href="{{ $switch_to_1_url }}/{{ $linked_report->slug }}">
					<div class=" report_thread">
						<div class="report_header">
							<div class="status">
								Status:
			                    @if($linked_report->closed != true)
			                    <span class="badge">{{ $status}}</span>
			                    @else
			                    <span class="closed badge">closed</span>
			                    @endif
							</div>
							<div class="report_created_at">
								{{ $linked_report->created_at->diffForHumans() }} <br>
								Views: {{ $linked_report->views($linked_report->slug) }}
							</div>
						</div>
						
						@if( $linked_report->photos && !$linked_report->photos->isEmpty() )
						<div class="report_thread_pics" >
							@foreach( $linked_report->photos as $photo )
							<img class="img img-responsive img-circle" 
			                    src="/{{ $photo->thumbnail_path }}" 
			                    alt="{{ $photo->item_description }}">
							@endforeach
						</div>

						@else
							@if( $report_type_url == '/missing_people')
							<img src="/img/user.jpg" class="img-circle img-responsive ">
							@else
							<img src="/img/{{$report->item_category}}.jpg" class="img-circle img-responsive ">
							@endif
						@endif
						<br>

						<div class="report_name">
							@if($report_level == 'item')
								{{ $linked_report->item_description }}
							@elseif($report_level == 'person')
								{{ $linked_report->person_name }}
							@endif
						</div>

						<div class="report_last_seen_date">
							<span>Last Seen:</span> {{ $linked_report->last_seen_date->format('d F Y') }}
							<br>

							in {{ $linked_report->last_seen_state }} around:
							@unless($linked_report->last_seen_areas->isEmpty())
				                @foreach($linked_report->last_seen_areas->pluck('name') as $last_seen_area )
				                <span class="badge">{{ $last_seen_area }}</span>
				                @endforeach
						    @endunless
						</div>
						
						<strong>reporter</strong>
						<table class="table table-condensed table-striped table-bordered table-hover">
							<tr>
								<td><i class="fa fa-at"></i></td> <td> {{ $linked_report->reporter_email }}</td>
							</tr>
							<tr>
								<td><i class="fa fa-phone"></i></td> <td> {{ $linked_report->reporter_phone }}</td>
							</tr>
						</table>
					</div>
				</a>
			</div>
        @endforeach
        
        @else
		<h1>There are no linked reports to this item</h1>
		@endif
      </div>
    </div>
  </div>
</div>
