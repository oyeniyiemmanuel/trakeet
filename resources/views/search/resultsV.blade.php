@extends('layouts.master')

@section('title', 'Search Results | Trakeet')

@section('side_navbar')

@endsection

@section('content')
		
		<div class="col-md-12 col-sm-12 col-xs-12 search_results_cover">
			<h1>
				Search Results 
				@if ( !empty($reports) )
				<span class="results_info_message">( {{ $total }} )</span>
				@endif
			</h1>
			<hr>
			@if ( empty($reports))

			<div class="results_error_message">{{ $error['error'] }}</div>
			<div class="trakeet_form">
				<form method="GET" action="/search" role="form">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-search"></i></div>
						<input class="form-control" type="search" name="query" value="{{$query}}">
						<div class="input-group-addon">
							<select onchange="this.form.submit()" class="" name="report_type">
								<option <?php echo $report_type=='App\Found_item' ? "selected": ''; ?> value="App\Found_item"> Found items</option>
								<option <?php echo $report_type=='App\Missing_item' ? "selected": ''; ?> value="App\Missing_item"> Missing items</option>
								<option <?php echo $report_type=='App\Missing_person' ? "selected": ''; ?> value="App\Missing_person"> Missing People</option>
							</select>
						</div>
					</div>
				</form>
			</div>
				
			@else

			<div class="trakeet_form">
				<form method="GET" action="/search" role="form">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-search"></i></div>
						<input class="form-control" type="search" name="query" value="{{$query}}">
						<div class="input-group-addon">
							<select onchange="this.form.submit()" class="" name="report_type">
								<option <?php echo $report_type=='App\Found_item' ? "selected": ''; ?> value="App\Found_item"> Found items</option>
								<option <?php echo $report_type=='App\Missing_item' ? "selected": ''; ?> value="App\Missing_item"> Missing items</option>
								<option <?php echo $report_type=='App\Missing_person' ? "selected": ''; ?> value="App\Missing_person"> Missing People</option>
							</select>
						</div>
					</div>
					<hr>
					<div><button class="btn btn-success" type="submit">Search</button></div>
				</form>
			</div>

				@foreach( $reports as $report )
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-2 col-sm-2"></div>
						
						@if( $report_type=='App\Found_item' )
						<a href="/found_items/{{ $report->slug }}">
						@elseif( $report_type=='App\Missing_item' )
						<a href="/missing_items/{{ $report->slug }}">
						@else
						<a href="/missing_people/{{ $report->slug }}">
						@endif
						<div class="col-md-8 col-sm-8 col-xs-12 search_result">
							<div class="col-md-4 col-sm-4 col-xs-4 image">
								@if( $report->photos && !$report->photos->isEmpty() )
								<div class="report_thread_pics" >
									@foreach( $report->photos as $photo )
									<img class="img img-responsive img-circle" 
					                    src="/{{ $photo->thumbnail_path }}" 
					                    alt="{{ $photo->item_description }}">
									@endforeach
								</div>
								@else
								<img src="/img/{{ $report->item_category }}.jpg" class="img-responsive img-circle img-thumbnail">
								@endif
							</div>
							<div class="col-md-8 col-sm-8 col-xs-8 details">
								@if( $report_type !== 'App\Missing_person' )
								<strong>Description:</strong>&nbsp; {{ substr($report->item_description, 0, 30) }}... <br>
								@else
								<strong>Description:</strong>&nbsp; {{ substr($report->person_description, 0, 30) }}... <br>
								@endif
								<strong>Reported by:</strong>&nbsp; {{ substr($report->reporter_name, 0, 30) }} <br>
								<strong>Date:</strong>&nbsp; {{ $report->created_at->format('d F Y') }} <br>
							</div>
						</div>
						</a>
						<div class="col-md-2 col-sm-2"></div>
					</div>
				@endforeach
				
			@endif

			<div class="pagination_cover col-md-12" style="z-index: 1">
			@if(!empty($reports))
				{{ $reports->links() }}
			@endif
			</div>

		</div>
		
@endsection

@section('footer')
	@parent
@endsection
		

