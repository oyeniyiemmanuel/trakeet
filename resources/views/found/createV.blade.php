@extends('layouts.master')

@section('title', 'Found an Item | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="form_cover">
		<div style="" class="row">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="trakeet_form col-md-8">
				
					<h2>Enter the details of the item found</h2>
					<hr>
					<!--display Validation Errors -->
					@include('errors.form_valid')
					
					<!--found items form-->
					{!! Form::open(['url'=>'found_items', 'role'=>'form', 'class'=>'']) !!}
						
						@include('partials.form', [
                                                     'submitButtonText'=>'Submit Report'
                                                     ,'select_state'=>'Select State Where item was found'
                                                     ,'select_area'=>'Area which item was found'
                                                     ,'select_date'=>'The date item was found'
                                                     ,'time_of_day'=>'What period of the day was the item found?'
                                                     ])

					{!! Form::close() !!}
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection
		

