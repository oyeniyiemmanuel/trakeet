@extends('layouts.master')

@section('title', 'Found Item | Trakeet')

@section('side_navbar')

@endsection

@section('content')
    
		@include('partials.item_focus', [
                                            'h1'=>'Found Items'
		                                    ,'switch_to_1'=>'missing items'
	                                    	,'switch_to_1_url'=>'/missing_items'
                                            ,'switch_to_2'=>'people'
	                                    	,'switch_to_2_url'=>'/missing_people'
		                                    ,'my_reports_url'=>'/users/found_items'
		                                    ,'report_type'=> $f_items
		                                    ,'report_type_url'=> '/found_items'
		                                    ,'comments'=> $comments
		                                    ,'status'=>'Reported Found'
		                                    ,'response_action_1' => 'yours?'
                                       		,'response_action_1_url' => 'claim_it'
                                       		,'select_date'=>'The date item was found'
                                       		,'img_url' => 'user.jpg'
                                       		,'report_level' => 'item'
		                                ]
		
		)
		
@endsection

@section('footer')
	@parent
@endsection
		

