@extends('layouts.master')

@section('title', 'Claim It | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
    <div class="form_cover">
		<div style="" class="row">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="trakeet_form col-md-8">
				
					<h2><strong><em>You've Been Looking For</em>: {{ $item->item_description }} </strong></h2>
					<hr>
					<!--display Validation Errors -->
					@include('errors.form_valid')
					
					<!--Missing items form-->
					{!! Form::open( ['url'=>'missing_items', 'role'=>'form']) !!}
						
						@include('partials.response_form', [
													'submitButtonText'=>'Link Report'
                                                     ,'select_state'=>'Select State Where item was lost'
                                                     ,'select_area'=>'Area which you think it got lost, or last saw it'
                                                     ,'select_date' => 'The date item was lost or last seen'
                                                     ,'time_of_day'=>'period of the day did you last saw the item?'
                                                     ,'response_action'=>'claim_it'
                                                  ])
						
					{!! Form::close() !!}
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection
		

