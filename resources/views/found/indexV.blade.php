@extends('layouts.master')

@section('title', 'Found Items | Trakeet')

@section('side_navbar')

@endsection

@section('content')
		
		@include('partials.items_index', [
		                                    'h1'=>'Found Items'
		                                    ,'switch_to_1'=>'missing items'
	                                    	,'switch_to_1_url'=>'/missing_items'
                                            ,'switch_to_2'=>'people'
	                                    	,'switch_to_2_url'=>'/missing_people'
		                                    ,'my_reports_url'=>'/users/found_items'
		                                    ,'report_type'=> $f_items
		                                    ,'report_type_url'=> '/found_items'
		                                    ,'response_action_1' => '<i class="fa fa-thumb-tack"></i>'
                                       		,'response_action_1_url' => 'claim_it'
                                       		,'status'=>'Reported Found'
                                       		,'img_url' => 'user.jpg'
                                       		,'report_level' => 'item'
		                                ]
		
		)
		
@endsection

@section('footer')
	@parent
@endsection
		

