@extends('layouts.master')

@section('title', 'Edit person report | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="form_cover">
		<div style="" class="row">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="trakeet_form col-md-8">
				
					<h2><strong><em>Edit Missing Person Post</em>: {{ $missing_person->person_name }} </strong></h2>
					<hr>
					<!--display Validation Errors -->
					@include('errors.form_valid')
					
					<!--missing people form-->
					{!! Form::model( $missing_person,
                                     ['method'=>'PATCH',
					                 'action'=>['MissingPeopleController@update', $missing_person->slug],
					                 'role'=>'form', 'class'=>'trakeet_form']) !!}
						
						@include('partials.missing_person_form', ['submitButtonText'=>'Update Report'])
						
					{!! Form::close() !!}
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection
		

