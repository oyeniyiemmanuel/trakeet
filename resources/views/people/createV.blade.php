@extends('layouts.master')

@section('title', 'Missing person | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="form_cover">
		<div style="" class="row">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="trakeet_form col-md-8">
				
					<h2>Enter the details of the missing Person</h2>
					<hr>
					<!--display Validation Errors -->
					@include('errors.form_valid')
					
					<!--found items form-->
					{!! Form::open(['url'=>'missing_people', 'role'=>'form', 'class'=>'']) !!}
						
						@include('partials.missing_person_form', ['submitButtonText'=>'Submit Report'])

					{!! Form::close() !!}
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection
		

