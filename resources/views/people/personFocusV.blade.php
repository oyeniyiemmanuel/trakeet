@extends('layouts.master')

@section('title', 'Missing Person | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
		
	@include('partials.person_focus', [
										'h1'=>'Missing People'
	                                    ,'switch_to_1'=>'missing items'
                                    	,'switch_to_1_url'=>'/missing_items'
                                        ,'switch_to_2'=>'found items'
                                    	,'switch_to_2_url'=>'/found_items'
										,'report_type_url'=>'/missing_people'
										,'comments'=> $comments
		                                ,'status'=>'Reported Missing'
		                                ,'response_action_1' => 'found the person'
                                       	,'response_action_1_url' => 'found_person'
                                       	,'img_url' => 'user.jpg'
									])

@endsection

@section('footer')
	@parent
@endsection
