@extends('layouts.master')

@section('title', 'People | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')

	@include('partials.people_index', [
		                                    'h1'=>'Missing people'
		                                    ,'switch_to_1'=>'found items'
	                                    	,'switch_to_1_url'=>'/found_items'
                                            ,'switch_to_2'=>'missing items'
	                                    	,'switch_to_2_url'=>'/missing_items'
	                                    	,'report_type' => $missing_people
		                                    ,'report_type_url'=> '/missing_people'
		                                    ,'response_action_1' => '<i class="fa fa-binoculars"></i>'
                                       		,'response_action_1_url' => ''
                                       		,'status'=>'Reported Missing'
                                       		,'img_url' => 'user.jpg'
											,'report_level' => 'person'
		                                ]
		
		)

@endsection

@section('footer')
	@parent
@endsection
