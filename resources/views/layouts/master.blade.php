<html>
	<head>
		<title>@yield('title')</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="trakeet" />
		<meta name="description" content="Trakeet is an app to for users to report missing and found items">
		<meta name="keywords" content="Trakeet, trakeet.com, found item, missing item, Nigeria">
		
		<link rel="stylesheet" href="{{ elixir('final/styles.css') }}">
		
	</head>
	<body>
		<div id="loader-wrapper">
            <!--<div id="loader"></div>-->
        </div>
		<div class="big-wrapper container-fluid">
			
			<!-- upper div section -->
			<div class="row upper_div">
				<!-- menu -->
				@section('side_navbar')
					<header class="cd-main-header">
						<a href="{{ env('APP_URL') }}" class="cd-logo"><!--<img src="/img/logo.jpg" alt="Trakeet">--><span>TRAKEET</span></a>
						
						<div class="cd-search is-hidden">
							<form method="GET" action="/search">
								<input type="search" name="query" placeholder="Search...">
								<input type="hidden" name="report_type" value="App\Found_item">
							</form>
						</div> 

						<a href="#0" class="cd-nav-trigger"><span></span></a>

						<nav class="cd-nav">
							<ul class="cd-top-nav">
								<li><a href="/guide"><i class="fa fa-map"></i> &nbsp; Guide </a></li>
								<li class="has-children account">
								@if (Auth::guest())
									<a href="#0">
										<i class="fa fa-user"></i>
										&nbsp;Account
									</a>

									<ul>

										<li><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> &nbsp;Login</a></li>
										<li><a href="{{ url('/register') }}"><i class="fa fa-user-plus"></i> &nbsp; Register</a></li> 
										<li><a href="{{ url('/official_id') }}"><i class="fa fa-user-secret"></i> &nbsp; Official</a></li> 
									</ul>
								@else
									@if( Auth::user()->is_official )
									<a href="#0">
										<i class="fa fa-user-secret"></i>
										&nbsp; {{ Auth::user()->name }} ( {{ Auth::user()->official()->office_type }} )
									</a>
									@else
								    <a href="#0">
										<i class="fa fa-user"></i>
										&nbsp; {{ Auth::user()->name }}
									</a>
									@endif
									<ul>

										<li><a href="#0"><i class="fa fa-cogs"></i> &nbsp; Settings</a></li>
										<li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> &nbsp;Logout</a></li>
									</ul>
								@endif
								</li>
							</ul>
						</nav>
					</header>

					<main class="cd-main-content">
						<nav class="cd-side-nav">
							<ul>
								@if(Auth::user())
								<li id="notifications" class="has-children notifications">
									<a data-token="{{ csrf_token() }}" class="mark_as_read" id="{{ Auth::id() }}">
										<em>Notifications</em>
										@if( !Auth::user()->unreadNotifications->isEmpty() )
										<span class="count">{{ count(Auth::user()->unreadNotifications) }}</span>
										@endif
									</a>
									
									<ul>
										<!-- trigger notifications modal -->
										<li>
											@if( !Auth::user()->notifications->isEmpty() )
											<a href="" class="view_all" data-toggle="modal" data-target="#all_notifications">
												<strong>VIEW ALL</strong>
											</a>
											@else
											<strong>No Notifications</strong>
											@endif
										</li>

										@foreach(Auth::user()->notifications->take(3) as $notification)
										<li>
											<a href="{{ $notification->data['link'] }}">
												@if( $notification->type == 'App\Notifications\NewLinkedReport' )
												<i class="fa fa-link"></i>&nbsp; 
												@elseif( $notification->type == 'App\Notifications\NewCommentOnReport' )
												<i class="fa fa-comment-o"></i>&nbsp; 
												@else
												<i class="fa fa-bell-o"></i>&nbsp; 
												@endif

												<?php echo substr($notification->data['short_content'], 0, 40) ?>...
												<span class="created_at">- {{ $notification->created_at->diffForHumans() }}</span>
											</a>
										</li>
										@endforeach
									</ul>
								</li>
								@endif
								<li class="has-children bookmarks active">
									<a><em>Filter via </em></a>
									
									<ul>
										<li><a href="{{ url('/missing_items/categories/automobile') }}"><i class="fa fa-automobile"></i>&nbsp; Automobiles</a></li>
										<li><a href="{{ url('/missing_people') }}"><i class="fa fa-users"></i>&nbsp; People</a></li>
										<li><a href="{{ url('/missing_items/categories/gadgets') }}"><i class="fa fa-tablet"></i>&nbsp; Gadgets</a></li>
										<li><a href="{{ url('/missing_items/categories/accessories') }}"><i class="fa fa-shopping-bag"></i>&nbsp;  Accessories</a></li>
										<li><a href="{{ url('/missing_items/categories/appliances') }}"><i class="fa fa-tv"></i>&nbsp; Appliances</a></li>
										<li><a href="{{ url('/missing_items/categories/others') }}"><i class="fa fa-money"></i>&nbsp; Other Items</a></li>
									</ul>
								</li>
								<li class="has-children overview active">
									<a><em>Missing Items<!--<span class="count">3</span>--></em></a>
									
									<ul>
										<li>
										    <a href="{{ url('/missing_items/create') }}">
                                                <i class="fa fa-bullhorn"></i>&nbsp; Report New
                                            </a>
								        </li>
										<li><a href="{{ url('/missing_items') }}"><i class="fa fa-list-alt"></i>&nbsp; Check List</a></li>
									</ul>
								</li>
								<li class="has-children overview active">
									<a><em>Found Items<!--<span class="count">3</span>--></em></a>
									
									<ul>
										<li>
										    <a href="{{ url('/found_items/create') }}">
										        <i class="fa fa-bullhorn"></i>&nbsp; Report New
								            </a>
								        </li>
										<li><a href="{{ url('/found_items') }}"><i class="fa fa-list-alt"></i>&nbsp; Check List</a></li>
									</ul>
								</li>
								<li class="has-children users active">
									<a><em>Missing People<!--<span class="count">3</span>--></em></a>
									
									<ul>
										<li>
										    <a href="{{ url('/missing_people/create') }}">
										        <i class="fa fa-bullhorn"></i>&nbsp; Report New
								            </a>
								        </li>
										<li><a href="{{ url('/missing_people') }}"><i class="fa fa-list-alt"></i>&nbsp; Check List</a></li>
									</ul>
								</li>

								<!--<li class="has-children comments">
									<a href="#0">Check Missing List From Authorities</a>
									
									<ul>
										<li><a href="#0">Police</a></li>
										<li><a href="#0">EFCC</a></li>
										<li><a href="#0">FRSC</a></li>
										<li><a href="#0">VIO</a></li>
									</ul>
								</li>-->
							</ul>
						</nav>

						<div class="content-wrapper">						
						
							<div class="general_jumbotron">
							<!-- display pane -->
							@yield('content')
							</div>

						</div> 
					</main>
			</div>
			
			<!-- lower div section -->
			@section('footer')
			<div class="row lower_div">
				<div class="col-md-2"></div>
				<div class="quicklinks col-md-10">
					<div class="col-md-2 col-sm-2"></div>
					<div class="col-md-2 col-sm-2">
						<strong>CATEGORIES</strong>
						<br><br>
						<ul>
							<li><a href="{{ url('/missing_items/categories/appliances') }}">Appliances</a></li>
							<li><a href="{{ url('/missing_items/categories/automobile') }}">Automobiles</a></li>
							<li><a href="{{ url('/missing_items/categories/gadgets') }}">Gadget</a></li>
							<li><a href="{{ url('/missing_items/categories/accessories') }}">Accessories</a></li>
							<li><a href="{{ url('/missing_items/categories/others') }}">Other items</a></li>
						</ul>
					</div>
					<div class="col-md-2 col-sm-2">
						<strong>SERVICES</strong>
						<br><br>
						<ul>
							<li><a href="{{ url('/missing_people') }}">People</a></li>
							<li><a href="{{ url('/found_items') }}">All found</a></li>
							<li><a href="{{ url('/missing_items') }}">All missing</a></li>
							<li><a href="{{ url('/found_items/create') }}">Found an item?</a></li>
							<li><a href="{{ url('/missing_items/create') }}">Lost an item?</a></li>
						</ul>
					</div>
					<div class="col-md-2 col-sm-2">
						<strong>TRAKEET</strong>
						<br><br>
						<ul>
							<li><a href="/guide">Guide</a></li>
							<li><a href="/testimonies">Testimonies</a></li>
							<li><a href="/statistics">Statistics</a></li>
							<li><a href="/faq">FAQ</a></li>
						</ul>
					</div>
					<div class="col-md-2 col-sm-2">
						<strong>LEGAL</strong>
						<br><br>
						<ul>
							<li><a href="/terms">Terms & Conditions</a></li>
							<li><a href="/privacy">Privacy & Cookies</a></li>
							<li><a href="/disclaimer">Disclaimer</a></li>
						</ul>
					</div>
					<div class="col-md-2 col-sm-2"></div>
					<div class="col-sm-12 copyright">
						Copyright &copy; 2016 Trakeet || All rights reserved || <a href="/disclaimer">disclaimer</a>
					</div>
				</div>
				
				
			</div>
			@show
		</div>
		<script type="text/javascript" src="{{ elixir('final/scripts.js') }}"></script>
		
		<!-- Display flash message  -->
		@include('partials.flash_message')
		<!-- include all general modals -->
		@include('modals.notifications')
		
	</body>
</html>