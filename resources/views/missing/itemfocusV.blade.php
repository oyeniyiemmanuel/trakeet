@extends('layouts.master')

@section('title', 'Missing Item | Trakeet')

@section('side_navbar')

@endsection

@section('content')
    
		@include('partials.item_focus', [
                                            'h1'=>'Missing Items'
		                                    ,'switch_to_1'=>'found items'
	                                    	,'switch_to_1_url'=>'/found_items'
                                            ,'switch_to_2'=>'people'
	                                    	,'switch_to_2_url'=>'/missing_people'
		                                    ,'my_reports_url'=>'/users/missing_items'
		                                    ,'report_type'=> $m_items
		                                    ,'report_type_url'=> '/missing_items'
		                                    ,'comments'=> $comments
		                                    ,'status'=>'Reported missing'
		                                    ,'response_action_1' => 'found it?'
                                       		,'response_action_1_url' => 'found_it'
                                       		,'select_date'=>'The date item was lost or last seen'
                                       		,'img_url' => 'user.jpg'
                                       		,'report_level' => 'item'
		                                ]
		
		)
		
@endsection

@section('footer')
	@parent
@endsection
		

