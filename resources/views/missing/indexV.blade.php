@extends('layouts.master')

@section('title', 'Missing Items | Trakeet')

@section('side_navbar')

@endsection

@section('content')
		
		@include('partials.items_index', [
		                                    'h1'=>'Missing Items'
		                                    ,'switch_to_1'=>'found items'
	                                    	,'switch_to_1_url'=>'/found_items'
                                            ,'switch_to_2'=>'people'
	                                    	,'switch_to_2_url'=>'/missing_people'
		                                    ,'my_reports_url'=>'/users/missing_items'
		                                    ,'report_type'=> $m_items
		                                    ,'report_type_url'=> '/missing_items'
		                                    ,'response_action_1' => '<i class="fa fa-map-pin"></i>'
                                       		,'response_action_1_url' => 'found_it'
                                       		,'status'=>'Reported missing'
                                       		,'img_url' => 'user.jpg'
                                       		,'report_level' => 'item'
		                                ]
		
		)
		
@endsection

@section('footer')
	@parent
@endsection
		

