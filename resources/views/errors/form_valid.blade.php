@if ($errors->any())
<div class="col-md-2"></div>
<div class="col-md-8">
    <ul class="alert alert-danger">
       <h3>OOOPS!</h3>
       <hr />
        @foreach ($errors->all() as $error)
            <li>
                <i class="fa fa-exclamation-circle"></i>
                &nbsp; {{ $error }} &nbsp;
            </li>
        @endforeach
    </ul>
</div>
<div class="col-md-2"></div>
<hr>
@endif