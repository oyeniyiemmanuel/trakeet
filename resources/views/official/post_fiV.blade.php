@extends('layouts.master')

@section('title', 'My Found Items | Trakeet')

@section('side_navbar')

@endsection

@section('content')
		<hr />
		<div style="color: black;" class="row">
			<div class="col-md-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<ul class="list-group">		
						<li class="list-group-item">
						    This page can only be seen by an Official
						</li>
					</ul>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
@endsection

@section('footer')
	@parent
@endsection
		

