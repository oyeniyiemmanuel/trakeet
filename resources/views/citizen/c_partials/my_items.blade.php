
    <div class="col-md-12">
        
        @include('partials.dashmenu')
        
        @if( $item_type1->isEmpty() && $item_type2->isEmpty() )
        <h3>There are no reports</h3>
        @else
        <div class="col-md-1"></div>
        <div class="col-md-10" style="padding: 0;">

            @include('partials.report', [
                                        'report_type' => $item_type1
                                        ])
                                        
             @include('partials.report', [
                                        'report_type' => $item_type2
                                        ])
        </div>
        <div class="col-md-1"></div>
        @endif
    </div>
