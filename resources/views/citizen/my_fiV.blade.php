@extends('layouts.master')

@section('title', 'My Found Items | Trakeet')

@section('side_navbar')

@endsection

@section('content')
    
		@include('citizen.c_partials.my_items', [
                                                 'h1'=>'Found Items'
                                                 ,'my_reports_url'=>'/users/found_items'
                                                 ,'report_type_url'=> '/found_items'
                                                 ,'switch_to_1'=>'missing items'
		                                    	 ,'switch_to_1_url'=>'/missing_items'
                                                 ,'switch_to_2'=>'people'
		                                    	 ,'switch_to_2_url'=>'/missing_people'
		                                         ,'item_type1'=>$f_items1
		                                         ,'item_type2'=>$f_items2
		                                         ,'status'=>'Reported Found'
		                                         ,'img_url' => 'user.jpg'
                                       			 ,'report_level' => 'item'
                                       			 ,'response_action_1' => '<i class="fa fa-thumb-tack"></i>'
                                       			 ,'response_action_1_url' => 'claim_it'
		                                         ]
		      )
		
@endsection

@section('footer')
	@parent
@endsection
		

