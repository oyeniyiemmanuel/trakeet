@extends('layouts.master')

@section('title', 'My Lost Items | Trakeet')

@section('side_navbar')

@endsection

@section('content')
    
		@include('citizen.c_partials.my_items', [
		                                         'h1'=>'Missing Items'
                                                 ,'my_reports_url'=>'/users/missing_items'
                                                 ,'report_type_url'=> '/missing_items'
		                                         ,'switch_to_1'=>'found items'
		                                    	 ,'switch_to_1_url'=>'/found_items'
                                                 ,'switch_to_2'=>'people'
		                                    	 ,'switch_to_2_url'=>'/missing_people'
		                                         ,'item_type1'=>$m_items1
		                                         ,'item_type2'=>$m_items2
		                                         ,'status'=>'Reported missing'
		                                         ,'img_url' => 'user.jpg'
                                       			 ,'report_level' => 'item'
                                       			 ,'response_action_1' => '<i class="fa fa-map-pin"></i>'
                                       			 ,'response_action_1_url' => 'found_it'
		                                         ]
		      )

@endsection

@section('footer')
	@parent
@endsection
		

