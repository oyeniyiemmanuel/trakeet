@extends('layouts.master')

@section('title', 'Official ID | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="form_cover">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="trakeet_form col-md-8">
				
					<h2>Enter your ID</h2>
					<hr>
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/official_id') }}">
                        {{ csrf_field() }}

						<div class="form-group">
					        {!! Form::label('official_code_no', 'Office ID number') !!}
					        <div class="input-group">
					        <div class="input-group-addon"><i class="fa fa-barcode"></i></div>
					        {!! Form::text('official_code_no', null, ['class'=>'form-control',
					                                           'placeholder'=>'you can only get this from your office']) !!}
					        </div>
					    </div>

					    <div class="form-group">
					        {!! Form::submit('proceed', ['class'=>'btn btn-success form-control']) !!}
					    </div>
				    </form>
				</div>
				<div class="col-md-2"></div>
			</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection
		

