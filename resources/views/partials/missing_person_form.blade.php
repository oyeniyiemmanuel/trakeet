	<div class="col-md-6 form-group">
        {!! Form::label('person_name', 'name') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-user"></i></div>
        {!! Form::text('person_name', null, ['class'=>'form-control',
                                           'placeholder'=>'']) !!}
        </div>
    </div>
    <div class="col-md-3 form-group">
        {!! Form::label('person_nickname', 'nickname (optional)') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-user-secret"></i></div>
        {!! Form::text('person_nickname', null, ['class'=>'form-control',
                                           'placeholder'=>'e.g. none']) !!}
        </div>
    </div>
    <div class="col-md-3 form-group">
        {!! Form::label('gender', 'gender') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-transgender-alt"></i></div>
        {!! Form::select('gender', 
                            [''=>'Select',
                             'male'=>'male',
                             'female'=>'female'],
                        null, 
                        ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('person_description', 'Describe the person') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-pencil-square-o"></i></div>
        {!! Form::textarea('person_description', null, ['class'=>'form-control',
                                                      'placeholder'=>'e.g. A tall dark male in his early 30s.',
                                                      'rows'=>'5']) !!}
        </div>
    </div>
    <div class="col-md-3 form-group">
        {!! Form::label('state_of_origin', 'state of origin') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-location-arrow"></i></div>
        {!! Form::select('state_of_origin', $states ,
                        null, 
                        ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3 form-group">
        {!! Form::label('local_government', 'Local Gov area') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
        {!! Form::text('local_government', null, ['class'=>'form-control',
                                           'placeholder'=>'']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_state', 'Last seen state') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-location-arrow"></i></div>
        {!! Form::select('last_seen_state', $states ,
                        null, 
                        ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_area_list', 'Last Seen Area') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
        {!! Form::select('last_seen_area_list[]', $last_seen_areas, null, ['multiple', 'id'=>'last_seen_area_list', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('distinct_feature_list', 'Distinct features') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-smile-o"></i></div>
        {!! Form::select('distinct_feature_list[]', $distinct_features, null, ['multiple', 'id'=>'distinct_features', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('disability_list', 'Disabilities') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-wheelchair"></i></div>
        {!! Form::select('disability_list[]', $disabilities, null, ['multiple', 'id'=>'disabilities', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('language_list', 'Languages') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-language"></i></div>
        {!! Form::select('language_list[]', $languages, null, ['multiple', 'id'=>'languages_spoken', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_date', 'The date he/she was last seen') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        @if( !empty($missing_person) )
        <input type="date" name="last_seen_date" class="form-control" placeholder="mm/dd/yyyy" required="required" value="{{ $missing_person->last_seen_date->format('Y-m-d') }}" max="{{ $missing_person->created_at->format('Y-m-d') }}" >
        @else
        <input type="date" name="last_seen_date" class="form-control" placeholder="mm/dd/yyyy" required="required" value="{{ date('Y-m-d') }}" max="{{ date('Y-m-d') }}">
        @endif
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_time', 'Time of day the person was last seen') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
        {!! Form::select('last_seen_time', 
                            [''=>'Select',
                             'dawn'=>'Dawn',
                             'morning'=>'Morning',
                             'about-noon'=>'About-noon',
                             'afternoon'=>'Afternoon',
                             'evening'=>'Evening',
                             'night'=>'Night'],
                        null, 
                        ['class'=>'form-control']) !!}
        </div>
    </div>
    
    <div class="col-md-12">
	    <h4>Reporter's details</h4>
		<hr>
	</div>
@if(Auth::guest())
    <div class="col-md-4 form-group">
        {!! Form::label('reporter_email', 'your email no ') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
        {!! Form::text('reporter_email', null, ['class'=>'form-control',
                                           'placeholder'=>'garba@mail.com']) !!}
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('reporter_name', 'your name') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-smile-o"></i></div>
        {!! Form::text('reporter_name', null, ['class'=>'form-control',
                                           'placeholder'=>'Ali Garba']) !!}
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('reporter_phone', 'your phone no' ) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
        {!! Form::text('reporter_phone', null, ['class'=>'form-control',
                                           'placeholder'=>'08012345678']) !!}
        </div>
    </div>
@else
    <div class="col-md-4 form-group">
        {!! Form::label('reporter_phone', 'your phone no' ) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
        {!! Form::text('reporter_phone', null, ['class'=>'form-control',
                                           'placeholder'=>'08012345678']) !!}
        </div>
    </div>
    {!! Form::hidden('reporter_email', Auth::user()->email ) !!}
    {!! Form::hidden('reporter_name', Auth::user()->name ) !!}
@endif
    <div class="col-md-12 form-group terms_and_conditions">
        {!! Form::checkbox('terms_and_conditions',1, null, ['id'=>'terms_and_conditions', 'required' => 'required']) !!}
        <label for="terms_and_conditions">
            <em><strong>I agree to <a href="/terms">trakeet terms</a> and <a href="/privacy">privacy policy</a></strong></em>
        </label>
    </div>

    <div class="col-md-12 form-group">
        {!! Form::submit($submitButtonText, ['class'=>'btn btn-success form-control']) !!}
    </div>