    <div class="col-md-6 form-group">
        {!! Form::label('item_category', 'Select type of item') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-sliders"></i></div>
        {!! Form::select('item_category', 
                            [''=>'Select',
                             'automobile'=>'Automobile',
                             'gadgets'=>'Gadgets',
                             'accessories'=>'Personal Accessories',
                             'appliances'=>'Appliances',
                             'other'=>'Other Items'],
                        null, 
                        ['class'=>'form-control']) !!}
        </div>
    </div>
     <div class="col-md-6 form-group">
        {!! Form::label('item_description', 'Describe the item') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-pencil-square-o"></i></div>
        {!! Form::textarea('item_description', null, ['class'=>'form-control',
                                                      'placeholder'=>'e.g. A brown wallet with green stripes.',
                                                      'rows'=>'2']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_state', $select_state) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-location-arrow"></i></div>
        {!! Form::select('last_seen_state', $states ,
                        null, 
                        ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_date', $select_date) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        @if( !empty($item))
        <input type="date" name="last_seen_date" class="form-control" placeholder="mm/dd/yyyy" required="required" value="{{ $item->last_seen_date->format('Y-m-d') }}" max="{{ $item->created_at->format('Y-m-d') }}" >
        @else
        <input type="date" name="last_seen_date" class="form-control" placeholder="mm/dd/yyyy" required="required" value="{{ date('Y-m-d') }}" max="{{ date('Y-m-d') }}" >
        @endif
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_area_list', $select_area) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
        {!! Form::select('last_seen_area_list[]', $last_seen_areas, null, ['multiple', 'id'=>'last_seen_area_list', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 form-group">
        {!! Form::label('last_seen_time', $time_of_day) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
        {!! Form::select('last_seen_time', 
                            [''=>'Select',
                             'dawn'=>'Dawn',
                             'morning'=>'Morning',
                             'about-noon'=>'About-noon',
                             'afternoon'=>'Afternoon',
                             'evening'=>'Evening',
                             'night'=>'Night'],
                        null, 
                        ['class'=>'form-control']) !!}
        </div>
    </div>
@if(Auth::guest())
    <div class="col-md-4 form-group">
        {!! Form::label('reporter_email', 'your email no ') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
        {!! Form::text('reporter_email', null, ['class'=>'form-control',
                                           'placeholder'=>'garba@mail.com']) !!}
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('reporter_name', 'your name') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-smile-o"></i></div>
        {!! Form::text('reporter_name', null, ['class'=>'form-control',
                                           'placeholder'=>'Ali Garba']) !!}
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('reporter_phone', 'your phone no' ) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
        {!! Form::text('reporter_phone', null, ['class'=>'form-control',
                                           'placeholder'=>'08012345678']) !!}
        </div>
    </div>
@else
    <div class="col-md-3"></div>
    <div class="col-md-6 form-group">
        {!! Form::label('reporter_phone', 'your phone no' ) !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
        {!! Form::text('reporter_phone', null, ['class'=>'form-control',
                                           'placeholder'=>'08012345678']) !!}
        </div>
    </div>
    <div class="col-md-3"></div>
    {!! Form::hidden('reporter_email', Auth::user()->email ) !!}
    {!! Form::hidden('reporter_name', Auth::user()->name ) !!}
@endif
    <div class="col-md-12 form-group terms_and_conditions">
        {!! Form::checkbox('terms_and_conditions',1, null, ['id'=>'terms_and_conditions', 'required' => 'required']) !!}
        <label for="terms_and_conditions">
            <em><strong>I agree to <a href="/terms">trakeet terms</a> and <a href="/privacy">privacy policy</a></strong></em>
        </label>
    </div>

    <div class="col-md-12 form-group">
        {!! Form::submit($submitButtonText, ['class'=>'btn btn-success form-control']) !!}
    </div>
