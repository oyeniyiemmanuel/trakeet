    <div class="form-group">
        {!! Form::label('message', 'Enter your comment') !!}
        <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-pencil-square-o"></i></div>
        {!! Form::textarea('message', null, ['class'=>'form-control'
                                             ,'placeholder'=> $placeholder
                                             ,'rows'=>'4'
                                             ,'id'=>'message'
                                             ,'required'=>'required'
                                    ]) !!}
        @if(Auth::user())
        {!! Form::hidden('user_id', Auth::user()->id, ['id'=>'user_id'] ) !!}
        @endif
        {!! Form::hidden('item_id', $report_id, ['id'=>'item_id'] ) !!}
        {!! Form::hidden('item_type', $report_type, ['id'=>'item_type'] ) !!}
        </div>
    </div>
@if(Auth::user())
    <div class="form-group">
        {!! Form::submit($submitCommentButton, ['class'=>'btn btn-success form-control', 'id'=>'submitCommentButton']) !!}
    </div>
@else
    <div class="form-group">
        <div class='btn btn-success disabled form-control'>
            Sorry, you are not logged in
        </div>
    </div>
@endif