 <div class="col-md-12">  
    @include('partials.dashmenu')
    
    @if( $report_type->isEmpty() )
    <h3>There are no reports</h3>
    @else
	<div class="col-md-1"></div>
	<div class="col-md-10" style="padding: 0;">
        
		@include('partials.report')
	    
	</div>
	<div class="col-md-1"></div>
	@endif
	

	<div class="pagination_cover col-md-12" style="z-index: 1">
		{{ $report_type->links() }}
	</div>
</div>