@foreach( $report_type as $report )
<div class="col-md-4 col-sm-6 report_cover">
		
		<div class="report_thread">
			<div class="report_header">
				<div class="status">
					Status:
                    @if($report->closed != true)
                    <span class="badge">{{ $status}}</span>
                    @else
                    <span class="closed badge">closed</span>
                    @endif <br>

                    @if( $report_type_url !== '/missing_people')
                    Category:
					{{ $report->item_category }}
                    @endif
				</div>
				<div class="report_created_at">
					{{ $report->created_at->diffForHumans() }} <br>
					Views: {{ $report->views($report->slug) }}
				</div>
			</div>
			<!--  response actions -->
			<div class="report_response_actions">
				<!-- first response action -->
				<a class="action response1" 
					data-toggle="tooltip" 
					title="<?php $a = explode('_', $response_action_1_url); echo implode(' ', $a); ?>" 
					href="{{ $report_type_url }}/{{ $report->slug }}/{{ $response_action_1_url }}">
					 <?php echo $response_action_1  ?>
				</a>
				
				<!-- social networks share buttons -->
				<span class="dropdown">
					<span class="dropdown-toggle" type="button" data-toggle="dropdown">
		           		<a class="action share" data-toggle="tooltip" title="Share" ><i class="fa fa-share-alt"></i></a>
		           </span>
		            <div class="dropdown-menu dropdown-menu-right social-buttons">
		                <div>
		                	<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(env('APP_URL').$report_type_url.'/'.$report->slug) }}" target="_blank">
		                		<i class="fa fa-facebook-square"></i>
		                	</a>
		                	<a href="https://twitter.com/intent/tweet?url={{ urlencode(env('APP_URL').$report_type_url.'/'.$report->slug) }}" target="_blank">
		                		<i class="fa fa-twitter-square"></i>
		                	</a>
		                	<a href="https://plus.google.com/share?url={{ urlencode(env('APP_URL').$report_type_url.'/'.$report->slug) }}" target="_blank">
		                		<i class="fa fa-google-plus-square"></i>
		                	</a>
		                	<a href="whatsapp://send?text={{ urlencode(env('APP_URL').$report_type_url.'/'.$report->slug) }}" data-action="share/whatsapp/share">
		                		<i class="fa fa-whatsapp"></i>
		                	</a>
		                </div>
		            </div>
		        </span>

				@if($report->linked && !$report->linked->isEmpty())
				<!-- trigger linked reports modal -->
				<span data-toggle="modal" data-target="#report_{{ $report->id }}">
					<a class="action link" data-toggle="tooltip" title="Linked Reports" >
						<i class="fa fa-link"></i>
						<em class="count_linked_reports_badge">{{ count($report->linked) }}</em>
					</a>
				</span>
				<!-- linked reports modal -->
				@include('modals.linked_reports')
	                
			    @endif

			    @if(Auth::user() && Auth::user()->email == $report->reporter_email)
			    <!-- close report action -->
			    	@if( $report->closed != true )
						<a class="action close_report" data-toggle="tooltip" title="Close Report" href="{{ $report_type_url }}/{{ $report->slug }}/close_report"> <i class="fa fa-lock"></i></a>
					@else
						<a class="action close_report" data-toggle="tooltip" title="Open Report" href="{{ $report_type_url }}/{{ $report->slug }}/open_report"> <i class="fa fa-unlock"></i></a>
					@endif
			    @endif
			    
			</div>
			
			<!-- report images -->
			<a href="{{ $report_type_url }}/{{ $report->slug }}">
				@if( $report->photos && !$report->photos->isEmpty() )
				<div class="report_thread_pics" >
					@foreach( $report->photos as $photo )
					<img class="img img-responsive img-circle" 
	                    src="/{{ $photo->thumbnail_path }}" 
	                    alt="{{ $photo->item_description }}">
					@endforeach
				</div>

				@else

					@if( $report_type_url == '/missing_people')
					<img src="/img/user.jpg" class="img-circle img-responsive ">
					@else
					<img src="/img/{{$report->item_category}}.jpg" class="img-circle img-responsive ">
					@endif

				@endif
				<br>
			</a>
			
			<!-- report name -->
			<a href="{{ $report_type_url }}/{{ $report->slug }}">
				<div class="report_name">
					@if($report_level == 'item')
						{{ substr($report->item_description, 0, 20) }} ...
					@elseif($report_level == 'person')
						{{ substr($report->person_name, 0, 20) }}
					@endif
				</div>
			</a>

			<!-- report last seen -->
			<div class="report_last_seen_date">
				<strong>Last Seen:</strong> {{ $report->last_seen_date->format('d F Y') }}
				<br>

				in 
				@unless($report->last_seen_areas->isEmpty())
	                @foreach($report->last_seen_areas->pluck('name') as $last_seen_area )
	                <span class="badge">{{ $last_seen_area }}</span>
	                @endforeach
			    @endunless, 
				<span class="report_last_seen_state">{{ $report->last_seen_state }} State</span>
			</div>
			
		</div>
	
</div>
@endforeach