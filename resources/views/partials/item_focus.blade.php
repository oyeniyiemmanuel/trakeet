
<div class="col-md-12 report_focus">
    <!-- include the dashmenu -->
    @include('partials.dashmenu')
    <h3>{{ strtoupper($item->item_description)}}</h3>

    <div class="col-md-3 col-sm-12 col-xs-12"></div>
    @if( !$item->photos->isEmpty() )
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div id="item_images" class="col-md-12 col-sm-12 col-xs-12">
            @foreach( $item->photos as $photo )
                    <div class="item_image_thumbnail col-md-6 col-sm-6 col-xs-6">
                        @if( Auth::user() && Auth::user()->email == $item->reporter_email )
                        <form id="_photo_{{ $photo->id }}"
                             class="delete_photo_form" 
                             method="POST" 
                             action="{{ $report_type_url }}_photo/{{ $photo->id }}">

                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                            <input id="photo_id" type="hidden" name="photo_id" value="{{ $photo->id }}">
                            <input id="item_type" type="hidden" name="item_type" value="{{ $report_type_url }}">
                            <span data-toggle="tooltip" title="Delete" class="btn btn-default delete_button">
                                <i class="fa fa-trash"></i>
                            </span>
                        </form>
                        @endif
                        <img class="img img-responsive img-circle" 
                            src="/{{ $photo->thumbnail_path }}" 
                            data-original-src="/{{ $photo->path }}" 
                            alt="{{ $item->item_description }}">
                    </div>

            @endforeach
        </div>
    </div>
    @else
    <div class="col-md-6">
        <div class="default_img">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <img src="/img/{{ $item->item_category }}.jpg" class="img-circle img-responsive ">
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    @endif
    <div class="col-md-3 col-sm-12 col-xs-12"></div>
               
    <ul class="col-md-12 nav nav-tabs nav-justified">
        <li class="active"><a data-toggle="tab" class="btn btn-default" aria-expanded="true" href="#details">Details</a></li>
        <li><a data-toggle="tab" class="btn btn-default" href="#talks"><i class="fa fa-comments-o"></i> Comments </a></li>
        @if( Auth::user() && Auth::user()->email == $item->reporter_email )
        <li><a data-toggle="tab" class="btn btn-default" href="#edit"> <i class="fa fa-edit"></i> Edit </a></li>
        <li data-toggle="tooltip" title="Add photos"><a data-toggle="tab" class="btn btn-default" href="#add_photos"> <i class="fa fa-plus"></i> <i class="fa fa-picture-o"></i></a></li>
        @endif
    </ul>
    
    <div class="tab-content">
        <div id="details" class="tab-pane fade  in active">
                <div class="col-md-4">
                    <table class="table table-condensed table-hover">
                        <tr><td>
                            <span class="left">Status:</span> 
                            <span class="right">
                                @if($item->closed != true)
                                <span class="badge">{{ $status}}</span>
                                @else
                                <span class="closed badge">closed</span>
                                @endif
                            </span>
                        </td></tr>
                        
                        <tr><td>
                            <span class="left">Category</span> 
                            <span class="right">{{ $item->item_category}}</span>
                        </td></tr>

                        <tr><td>
                            <span class="left">
                                 @if($report_type_url == '/found_items')
                                    Was found in
                                @else
                                    Was lost in
                                @endif

                                @unless($item->last_seen_areas->isEmpty())

                                    @foreach($item->last_seen_areas->pluck('name') as $last_seen_area)
                                        <span class="badge">{{ $last_seen_area }}</span>
                                    @endforeach

                                @endunless(),

                                {{ $item->last_seen_state }} about 

                                 {{ $item->last_seen_date->diffForHumans()}} and reported {{ $item->created_at->diffForHumans() }}
                            </span>
                        </td></tr>
                        
                        <tr><td>
                            <span class="left">Last seen</span> 
                            <span class="right">{{ $item->last_seen_date->format('d F Y')}}</span>
                        </td></tr>

                        <tr><td>
                            <span class="left">Period of day</span> 
                            <span class="right">{{ $item->last_seen_time}}</span>
                        </td></tr>

                        <tr><td>
                            <span class="left">Views</span> 
                            <span class="right">{{ $item->views($item->slug)}}</span>
                        </td></tr>

                        <tr><td>
                            <span class="left">Linked Reports</span> 
                            <span class="right">{{ count($item->linked)}}</span>
                        </td></tr>

                        <tr><td>
                            <span class="left">Reporter</span> 
                            <span class="right">{{ $item->reporter_name}}</span>
                        </td></tr>

                        <tr><td>
                            <span class="left"> email</span> 
                            <span class="right">{{ $item->reporter_email}}</span>
                        </td></tr>

                        <tr><td>
                            <span class="left">phone no</span> 
                            <span class="right">{{ $item->reporter_phone}}</span>
                        </td></tr>
                    </table>
                </div>

                <div class="col-md-8 playground">
                @if( $report_type_url == '/found_items' )
                    <h1>Was this item yours?</h1>
                    <div>
                        Notify the finder by pining a report 
                        <a class="btn btn-success action response1" 
                            href="{{ $report_type_url }}/{{ $item->slug }}/{{ $response_action_1_url }}">
                            <i class="fa fa-thumb-tack"></i>
                        </a>
                    </div> 
                @else
                    <h1>Seen this item?</h1>
                    <div>
                        Notify the reporter by pining a report 
                        <a class="btn btn-success action response1" 
                            href="{{ $report_type_url }}/{{ $item->slug }}/{{ $response_action_1_url }}">
                            <i class="fa fa-map-pin"></i>
                        </a>
                    </div> 
                @endif

                    <h1>Share on socials</h1>
                    <div class="social-buttons">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(env('APP_URL').$report_type_url.'/'.$item->slug) }}" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="https://twitter.com/intent/tweet?url={{ urlencode(env('APP_URL').$report_type_url.'/'.$item->slug) }}" target="_blank">
                            <i class="fa fa-twitter-square"></i>
                        </a>
                        <a href="https://plus.google.com/share?url={{ urlencode(env('APP_URL').$report_type_url.'/'.$item->slug) }}" target="_blank">
                            <i class="fa fa-google-plus-square"></i>
                        </a>
                        <a href="whatsapp://send?text={{ urlencode(env('APP_URL').$report_type_url.'/'.$item->slug) }}" data-action="share/whatsapp/share">
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </div>

                    <h1>Check linked reports</h1>
                    <div>
                        You can view the list of previously pinned reports to this item 
                        <!-- trigger linked reports modal -->
                        <section data-toggle="modal" data-target="#report_{{ $item->id }}">
                            <a class="action link">
                                <em class="count_linked_reports_badge">{{ count($item->linked) }}</em>
                                <i class="fa fa-link"></i>
                                
                            </a>
                        </section>

                        
                    </div>
                </div>
        </div>
        
        <div id="talks" class="tab-pane fade">
            <div class="col-md-12 comments_box">
               <div id="showComments">
                       @include('partials.comments')
               </div>
                <div class="">	
                        {!! Form::open( ['url'=>'/comments', 'role'=>'form', 'class'=>'trakeet_form', 'id'=>'createComment'] ) !!}
                            
                            @if( $report_type_url == '/missing_items' )
                            @include('partials.comments_form', [
                                                                'submitCommentButton'=>'Submit Comment'
                                                                ,'report_type'=>'/missing_items'
                                                                ,'placeholder'=>'Want to say something about the item?'
                                                                ,'report_id'=> $item->id
                                                                ])
                            @endif
                            
                            @if( $report_type_url == '/found_items' )
                            @include('partials.comments_form', [
                                                                'submitCommentButton'=>'Submit Comment'
                                                                ,'report_type'=>'/found_items'
                                                                ,'placeholder'=>'Want to say something about the item?'
                                                                ,'report_id'=> $item->id
                                                                ])
                            @endif
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
        
        <div id="edit" class="tab-pane fade form_cover">
            <div class="col-md-10">	
                    <!--display Validation Errors -->
                    @include('errors.form_valid')

                    @if( $report_type_url == '/found_items' )
                        <!--found items form-->
                        {!! Form::model( $item,
                                         ['method'=>'PATCH',
                                         'action'=>['FoundItemsController@update', $item->slug],
                                         'role'=>'form', 'class'=>'trakeet_form trakeet_specific_edit_form']) !!}

                            @include('partials.form', ['submitButtonText'=>'Update Report'
                                                         ,'select_state'=>'Select State Where item was found'
                                                         ,'select_area'=>'Area which item was found'
                                                         ,'time_of_day'=>'What period of the day was the item found?'
                                                         ,'item'=>$item
                                                         ])

                        {!! Form::close() !!}
                    @endif
                    
                    @if( $report_type_url == '/missing_items' )
                        <!--Missing items form-->
                        {!! Form::model( $item,
                                         ['method'=>'PATCH',
                                         'action'=>['MissingItemsController@update', $item->slug],
                                         'role'=>'form', 'class'=>'trakeet_form trakeet_specific_edit_form']) !!}

                            @include('partials.form', [
                                                                'submitButtonText'=>'Update Report'
                                                                 ,'select_state'=>'Select State Where item was lost'
                                                                 ,'select_area'=>'Area which you think it got lost, or last saw it'
                                                                 ,'time_of_day'=>'period of the day did you last saw the item?'
                                                                 ,'item'=>$item
                                                                ])

                        {!! Form::close() !!}
                    @endif
            </div>
            <div class="col-md-2">
                @if( Auth::user() && Auth::user()->email == $item->reporter_email )
                    <!-- close report action -->
                    @if( $item->closed != true )
                        <a class="btn btn-primary" data-toggle="tooltip" title="Close Report" href="{{ $report_type_url }}/{{ $item->slug }}/close_report"> <i class="fa fa-lock"></i> Close Report</a>
                    @else
                        <a class="btn btn-primary" data-toggle="tooltip" title="Open Report" href="{{ $report_type_url }}/{{ $item->slug }}/open_report"> <i class="fa fa-unlock"></i> Open Report</a>
                    @endif
                    <hr>
                    <form id="delete_report" class="" method="POST" action="{{ $report_type_url }}/{{ $item->slug }}">

                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="DELETE">
                        <input id="report_type" type="hidden" name="" value="{{ $report_type_url }}">
                        <input id="report_slug" type="hidden" name="" value="{{ $item->slug }}">
                        <span id="delete_report_button" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                            Delete Report
                        </span>
                    </form>
                    @endif
            </div>
        </div>
        <div id="add_photos" class="tab-pane fade">
            <div class="col-md-12">      
                <h4>Add Pictures</h4>
                <form id="addPhotosForm" class="dropzone" method="POST" action="{{ url($report_type_url.'/'.$item->slug.'/photos') }}">
                    {{ csrf_field() }}
                    <div class="dz-message">
                        Drag pictures here or click to upload <br>
                        <span>( Maximum files = 2 )</span>
                    </div>
                </form>
            </div>
        </div>

    </div>
            
</div>

<!-- linked reports modal -->
@include('modals.linked_reports', ['report' => $item])