<div class="col-md-12 dashmenu">

    <nav class="navbar navbar-default" data-spy="affix" data-offset-top="45">
       <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#<?php $a= explode('/', $report_type_url); echo implode('', $a); ?>">
            <i class="fa fa-angle-double-down"></i>
          </button>
          <div class="navbar-brand">{{ $h1 }}</div>
        </div>
        <div class="collapse navbar-collapse" id="<?php $a= explode('/', $report_type_url); echo implode('', $a); ?>">
          <ul class="nav navbar-nav">
            <li><a href="{{ $report_type_url }}">All</a></li>
            <li>
                <div class="dropdown">
                   <a class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-map-marker"></i> State
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach ( $states as $state )
                          <li><a href="{{ url($report_type_url.'/states/'.$state) }}">{{ $state }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </li>

            @if( $report_type_url != '/missing_people' )
            <li>
                <div class="dropdown">
                   <a class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-sliders"></i> Category
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url($report_type_url.'/categories/automobile') }}">Automobile</a></li>
                        <li><a href="{{ url($report_type_url.'/categories/gadgets') }}">Gadgets</a></li>
                        <li><a href="{{ url($report_type_url.'/categories/accessories') }}">Personal Accessories</a></li>
                        <li><a href="{{ url($report_type_url.'/categories/appliances') }}">Appliances</a></li>
                        <li><a href="{{ url($report_type_url.'/categories/others') }}">Other items</a></li>
                    </ul>
                </div>
            </li>

            @if( Auth::user())
            <li><a href="{{ $my_reports_url }}">My Items</a></li> 
            @endif

            @endif

            <li><a href="{{ $report_type_url }}/create">New</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ $switch_to_1_url }}"><span class="switch_btn">{{ $switch_to_1 }}</span></a></li>
            <li><a href="{{ $switch_to_2_url }}"><span class="switch_btn">{{ $switch_to_2 }}</span></a></li>
          </ul>
        </div>
      </div>
    </nav>

   
</div>


