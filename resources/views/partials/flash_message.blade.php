@if(Session::has('flash_msg'))
<div class="alert alert-info {{ Session::has('flash_important') ? 'alert-important' : '' }}">

    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
        <i class="fa fa-close"></i>
    </button>

    <script type="text/javascript">
		
		swal({
			title: "{{ session('flash_msg.title') }}",
			text: "{{ session('flash_msg.message') }}",
			type: "{{ session('flash_msg.level') }}",
			timer: 1500,
			showConfirmButton: false
		});

	</script>
	{{ session::forget('flash_msg') }}
</div>
@endif