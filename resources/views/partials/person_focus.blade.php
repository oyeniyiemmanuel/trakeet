
<div class="col-md-12 report_focus">
    <!-- include the dashmenu -->
    @include('partials.dashmenu')
    <h3>{{ strtoupper($missing_person->person_name)}}</h3>

    <div class="col-md-3 col-sm-12 col-xs-12"></div>
    @if( !$missing_person->photos->isEmpty() )
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div id="item_images" class="col-md-12">
            @foreach( $missing_person->photos as $photo )
                    <div class="item_image_thumbnail col-md-6 col-sm-6 col-xs-6">
                        @if( Auth::user() && Auth::user()->email == $missing_person->reporter_email )
                        <form id="_photo_{{ $photo->id }}"
                             class="delete_photo_form" 
                             method="POST" 
                             action="{{ $report_type_url }}_photo/{{ $photo->id }}">

                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                            <input id="photo_id" type="hidden" name="photo_id" value="{{ $photo->id }}">
                            <input id="item_type" type="hidden" name="item_type" value="{{ $report_type_url }}">
                            <span data-toggle="tooltip" title="Delete" class="btn btn-default delete_button">
                                <i class="fa fa-trash"></i>
                            </span>
                        </form>
                        @endif
                        <img class="img img-responsive img-circle" 
                            src="/{{ $photo->thumbnail_path }}" 
                            data-original-src="/{{ $photo->path }}" 
                            alt="{{ $missing_person->item_description }}">
                    </div>

            @endforeach
        </div>
    </div>
    @else
    <div class="col-md-6">
        <div class="default_img">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <img src="/img/{{ $img_url }}" class="img-circle img-responsive ">
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    @endif
    <div class="col-md-3 col-sm-12 col-xs-12"></div>

    <ul class="col-md-12 nav nav-tabs nav-justified">
        <li class="active"><a data-toggle="tab" class="btn btn-default" aria-expanded="true" href="#details">Details</a></li>
        <li><a data-toggle="tab" class="btn btn-default" href="#talks"><i class="fa fa-comments-o"></i> Comments </a></li>
        @if( Auth::user() && Auth::user()->email == $missing_person->reporter_email )
        <li><a data-toggle="tab" class="btn btn-default" href="#edit"> <i class="fa fa-edit"></i> Edit </a></li>
        <li data-toggle="tooltip" title="Add photos"><a data-toggle="tab" class="btn btn-default" href="#add_photos"> <i class="fa fa-plus"></i> <i class="fa fa-picture-o"></i></a></li>
        @endif
    </ul>

    <div class="tab-content">
        <div id="details" class="tab-pane fade  in active">
            <div class="col-md-4">
                <table class="table table-condensed table-hover">
                    <tr><td>
                        <span class="left">Status:</span> 
                        <span class="right">
                            @if($missing_person->closed != true)
                            <span class="badge">{{ $status}}</span>
                            @else
                            <span class="closed badge">closed</span>
                            @endif
                        </span>
                    </td></tr>
                    
                    <tr><td>
                        <span class="left">Full Name</span> 
                        <span class="right">{{ $missing_person->person_name}}</span>
                    </td></tr>

                    <tr><td>
                        <span class="left">Nickname</span> 
                        <span class="right">{{ $missing_person->person_nickname}}</span>
                    </td></tr>

                    <tr><td>
                        <span class="left">Gender</span> 
                        <span class="right">{{ $missing_person->gender}}</span>
                    </td></tr>

                    <tr><td>
                        <span class="left">Description</span> 
                        <span class="right">{{ $missing_person->person_description}}</span>
                    </td></tr>

                    <tr><td>
                        <span class="left">State Of Origin</span> 
                        <span class="right">{{ $missing_person->state_of_origin}}</span>
                    </td></tr>

                    <tr><td>
                        <span class="left">Local Government</span> 
                        <span class="right">{{ $missing_person->local_government}}</span>
                    </td></tr>
                    
                    <tr><td>
                        <span class="left">Distinct Features</span> 
                        <span class="right">
                            @unless($missing_person->distinct_features->isEmpty())

                                @foreach($missing_person->distinct_features->pluck('name') as $distinct_feature)
                                    {{ $distinct_feature }}
                                @endforeach

                            @endunless()
                        </span>
                    </td></tr>
                    
                    <tr><td>
                        <span class="left">Disabilities</span> 
                        <span class="right">
                            @unless($missing_person->disabilities->isEmpty())

                                @foreach($missing_person->disabilities->pluck('name') as $disability)
                                    {{ $disability }}
                                @endforeach

                            @endunless()
                        </span>
                    </td></tr>
                    
                    <tr><td>
                        <span class="left">Languages</span> 
                        <span class="right">
                            @unless($missing_person->languages->isEmpty())

                                @foreach($missing_person->languages->pluck('name') as $language)
                                    {{ $language }}
                                @endforeach

                            @endunless()
                        </span>
                    </td></tr>
                    
                    <tr><td>
                        <span class="left">Last Seen Date</span> 
                        <span class="right">{{ $missing_person->last_seen_date->format('d F Y')}}</span>
                    </td></tr>

                    <tr><td>
                        <span class="left">Period of day</span> 
                        <span class="right">{{ $missing_person->last_seen_time}}</span>
                    </td></tr>

                    <tr><td>
                        <span class="left">Views</span> 
                        <span class="right">{{ $missing_person->views($missing_person->slug)}}</span>
                    </td></tr>
                </table>
            </div>
            
            <div class="col-md-8 playground">
                <h1>Have you seen someone who matches @if( $missing_person->gender == 'male' )
                                        his
                                        @else
                                        her 
                                        @endif description?
                </h1>
                <div>
                    Notify {{ $missing_person->reporter_name}} via email - {{ $missing_person->reporter_email}} or phone - {{ $missing_person->reporter_phone}}
                </div> 
                
                <h1>Or help find <?php $first_name = explode(' ', $missing_person->person_name); 
                                    echo ucfirst(end($first_name)); 
                                    ?> by sharing on</h1>
                <div class="social-buttons">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(env('APP_URL').$report_type_url.'/'.$missing_person->slug) }}" target="_blank">
                    <i class="fa fa-facebook-square"></i>
                    </a>
                    <a href="https://twitter.com/intent/tweet?url={{ urlencode(env('APP_URL').$report_type_url.'/'.$missing_person->slug) }}" target="_blank">
                        <i class="fa fa-twitter-square"></i>
                    </a>
                    <a href="https://plus.google.com/share?url={{ urlencode(env('APP_URL').$report_type_url.'/'.$missing_person->slug) }}" target="_blank">
                        <i class="fa fa-google-plus-square"></i>
                    </a>
                    <a href="whatsapp://send?text={{ urlencode(env('APP_URL').$report_type_url.'/'.$missing_person->slug) }}" data-action="share/whatsapp/share">
                        <i class="fa fa-whatsapp"></i>
                    </a>
                </div>

                <h4>
                    @if( $missing_person->gender == 'male' )
                    He
                    @else
                    She 
                    @endif was last seen in

                     {{ $missing_person->last_seen_state }} around 

                    @unless($missing_person->last_seen_areas->isEmpty())

                        @foreach($missing_person->last_seen_areas->pluck('name') as $last_seen_area)
                            <span class="badge">{{ $last_seen_area }}</span>
                        @endforeach

                    @endunless()

                     {{ $missing_person->last_seen_date->diffForHumans()}} and reported {{ $missing_person->created_at->diffForHumans() }}
                </h4>
            </div>
        </div>
        
        <div id="talks" class="tab-pane fade">
            <div class="col-md-12 comments_box">
               <div id="showComments">
                     @include('partials.comments')  
               </div>
                <div class="">  
                        {!! Form::open( ['url'=>'/comments', 'role'=>'form', 'class'=>'trakeet_form', 'id'=>'createComment'] ) !!}
                            
                            @include('partials.comments_form', [
                                                                'submitCommentButton'=>'Submit Comment'
                                                                ,'report_type'=>'/missing_people'
                                                                ,'placeholder'=>'Want to say something about the missing person?'
                                                                ,'report_id'=> $missing_person->id
                                                                ])

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
        
        <div id="edit" class="tab-pane fade form_cover">
            <div class="col-md-10">	
                   <!--display Validation Errors -->
                    @include('errors.form_valid')

                    <!--missing people form-->
                    {!! Form::model( $missing_person,
                                     ['method'=>'PATCH',
                                     'action'=>['MissingPeopleController@update', $missing_person->slug],
                                     'role'=>'form', 'class'=>'trakeet_form trakeet_specific_edit_form']) !!}
                        
                        @include('partials.missing_person_form', ['submitButtonText'=>'Update Report'])
                        
                    {!! Form::close() !!}
            </div>
            <div class="col-md-2">
                @if( Auth::user() && Auth::user()->email == $missing_person->reporter_email )
                    <!-- close report action -->
                @if( $missing_person->closed != true )
                    <a class="btn btn-primary" data-toggle="tooltip" title="Close Report" href="{{ $report_type_url }}/{{ $missing_person->slug }}/close_report"> <i class="fa fa-lock"></i> Close Report</a>
                @else
                    <a class="btn btn-primary" data-toggle="tooltip" title="Open Report" href="{{ $report_type_url }}/{{ $missing_person->slug }}/open_report"> <i class="fa fa-unlock"></i> Open Report</a>
                @endif
                <hr>
                <form id="delete_report" method="POST" action="{{ $report_type_url }}/{{ $missing_person->slug }}">

                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <input id="report_type" type="hidden" name="" value="{{ $report_type_url }}">
                    <input id="report_slug" type="hidden" name="" value="{{ $missing_person->slug }}">
                    <span id="delete_report_button" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                        Delete Report
                    </span>
                </form>
                @endif
            </div>
        </div>
        <div id="add_photos" class="tab-pane fade">
            <div class="col-md-12">      
                <h4>Add Pictures</h4>
                <form id="addPhotosForm" class="dropzone" method="POST" action="{{ url($report_type_url.'/'.$missing_person->slug.'/photos') }}">
                    {{ csrf_field() }}
                    <div class="dz-message">
                        Drag pictures here or click to upload <br>
                        <span>( Maximum files = 2 )</span>
                    </div>
                </form>
            </div>
        </div>

    </div>
        
</div>

