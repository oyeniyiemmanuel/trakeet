
	    <div class="form-group comment_li">
	        <strong class="comment_user"> {{ App\User::findOrFail($new_comment->user_id)->name }} </strong>
	        @if(Auth::user() && Auth::user()->id == $new_comment->user_id)

            <input type="hidden" name="id" id="comment_id" value="{{ $new_comment->id }}">
            <input type="hidden" name="item_type" id="item_type" value="{{ $item_type_url }}">
            <button type="button" class="comment_delete" data-token="{{ csrf_token() }}"> Delete </button>

            @endif
	        <br />

	        <div class="comment_message col-md-8">
	        	{{ $new_comment->message }}
			</div>
	        <strong class="comment_time">
	        {{ $new_comment->created_at->diffForHumans() }}
	        </strong>
	    </div>
	    
