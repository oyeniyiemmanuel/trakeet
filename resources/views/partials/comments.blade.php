    @foreach ($comments as $comment)
        <div class="form-group comment_li">
            <strong class="comment_user"> {{ App\User::findOrFail($comment->user_id)->name }} </strong> 
            @if(Auth::user() && Auth::user()->id == $comment->user_id)

            <input type="hidden" name="id" id="comment_id" value="{{ $comment->id }}">
            <input type="hidden" name="item_type" id="item_type" value="{{ $report_type_url }}">
            <button type="button" class="comment_delete" data-token="{{ csrf_token() }}"> Delete </button>
            

            @endif
            <br />

            <div class="comment_message col-md-8">
                {{ $comment->message }}
            </div>
            <strong  class="comment_time">
            {{ $comment->pivot->created_at->diffForHumans() }}
            </strong>
        </div>
    @endforeach
