@extends('layouts.master')

@section('title', 'Trakeet | Home')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12 welcome">
		<div class="col-md-6 col-xs-12 items_intro">
			<h1>Lost an item? <a href="{{ url('/found_items/create') }}"><span>or found one</span></a></h1>
			<div class="col-md-12 trakeet_form">
				
				<h2>Enter the details of what you lost</h2>
				<hr>
				<!--display Validation Errors -->
				@include('errors.form_valid')
				
				<!--found items form-->
				{!! Form::open(['url'=>'missing_items', 'role'=>'form', 'class'=>'']) !!}
					
					@include('partials.form', [
                                                 'submitButtonText'=>'Submit Report'
                                                 ,'select_state'=>'Select State Where item was lost'
                                                 ,'select_area'=>'Area which you think it got lost, or last saw it'
                                                 ,'select_date'=>'The date item was lost or last seen'
                                                 ,'time_of_day'=>'period of the day did you last see the item?'
                                                 ])
					
				{!! Form::close() !!}
			</div>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 missing_person_intro">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1>Are you <strong>looking</strong> for a <em><strong>missing person</strong></em>?</h1>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<a href="{{ url('/missing_people/create') }}"><button class="btn"> Fill a report</button></a>
				<hr>
					<h2>It is submitted directly to the police</h2>
				<hr>
			</div>
		</div>

		<div class="homepage_reports col-md-12 col-sm-12 col-xs-12">
			<<div class="col-md-12">
				<h1><a href="{{ url('/missing_people') }}"><span>view all missing people reports</span></a></h1>
			</div>
			@include('partials.homepage_items', [
			                                    'h1'=>'Missing people'
			                                    ,'switch_to_1'=>'found items'
		                                    	,'switch_to_1_url'=>'/found_items'
	                                            ,'switch_to_2'=>'missing items'
		                                    	,'switch_to_2_url'=>'/missing_items'
		                                    	,'report_type' => $missing_people
			                                    ,'report_type_url'=> '/missing_people'
			                                    ,'response_action_1' => '<i class="fa fa-binoculars"></i>'
	                                       		,'response_action_1_url' => ''
	                                       		,'status'=>'Reported Missing'
	                                       		,'img_url' => 'user.jpg'
												,'report_level' => 'person'
			                                ]

			)
			<div class="col-md-12">
				<h1><a href="{{ url('/missing_people') }}"><span>view all missing people reports</span></a></h1>
			</div>
		</div>
	</div>
	
	<div class="col-md-12"><br></div>

	<div class="homepage_reports col-md-12 col-sm-12 col-xs-12">
	<h1>Missing Items <a href="{{ url('/missing_items') }}"><span>view all</span></a></h1>
	@include('partials.homepage_items', [
	                                    'h1'=>'Missing Items'
	                                    ,'switch_to_1'=>'found items'
	                                	,'switch_to_1_url'=>'/found_items'
	                                    ,'switch_to_2'=>'people'
	                                	,'switch_to_2_url'=>'/missing_people'
	                                    ,'my_reports_url'=>'/users/missing_items'
	                                    ,'report_type'=> $m_items
	                                    ,'report_type_url'=> '/missing_items'
	                                    ,'response_action_1' => '<i class="fa fa-map-pin"></i>'
	                               		,'response_action_1_url' => 'found_it'
	                               		,'status'=>'Reported missing'
	                               		,'img_url' => 'user.jpg'
	                               		,'report_level' => 'item'
	                                ]

	)
	</div>

	<div class="col-md-12"><br></div>
	
	<div class="homepage_reports col-md-12 col-sm-12 col-xs-12">
	<h1>Found Items <a href="{{ url('/found_items') }}"><span>view all</span></a></h1>
	@include('partials.homepage_items', [
	                                    'h1'=>'Found Items'
	                                    ,'switch_to_1'=>'missing items'
                                    	,'switch_to_1_url'=>'/missing_items'
                                        ,'switch_to_2'=>'people'
                                    	,'switch_to_2_url'=>'/missing_people'
	                                    ,'my_reports_url'=>'/users/found_items'
	                                    ,'report_type'=> $f_items
	                                    ,'report_type_url'=> '/found_items'
	                                    ,'response_action_1' => '<i class="fa fa-thumb-tack"></i>'
                                   		,'response_action_1_url' => 'claim_it'
                                   		,'status'=>'Reported Found'
                                   		,'img_url' => 'user.jpg'
                                   		,'report_level' => 'item'
	                                ]

	)
	</div>

	
@endsection

@section('footer')
	@parent
@endsection