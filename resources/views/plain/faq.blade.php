@extends('layouts.master')

@section('title', 'FAQ | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="faq">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1>Frequently Asked Questions</h1>
				<hr>
				<div class="col-md-2"></div>
				<div class="col-md-8 segment">
					<p class="question">Q: How do i edit a report?</p>
					<p class="answer"><em>A:</em> Register with the same email you reported with, then login. click on the report and there's an <strong>edit</strong> option available.
					</p>

					<p class="question">Q: When and how do i get notified about my report?</p>
					<p class="answer"><em>A:</em> you receive notifications whenever there's a new comment or a new link attached to your report. And you are notified via email, when you log in, and via text message.
					</p>
					
					<p class="question">Q: Deleting a report</p>
					<p class="answer"><em>A:</em> Register with the same email you reported with, then login. click on the report, navigate to <strong>edit</strong> and there's a <strong>delete report</strong> option available.
					</p>
					
					<p class="question">Q: How am i sure a reporter really has my item?</p>
					<p class="answer"><em>A:</em> ask them to pin a report and upload pictures of the item.
					</p>

					<p class="question">Q: How do i retrieve my item from the finder?</p>
					<p class="answer"><em>A:</em> ask them to drop it at any nearest police station. you can retrieve your item there. Trakeet strongly discourages meeting people at unsafe locations to retrieve an item ( as stated on Section 11.7 in our <em><a href="/terms">Terms & Conditions</a></em> )
					</p>

					<p class="question">Q: What is the difference between open and closed report?</p>
					<p class="answer"><em>A:</em> A closed report means the reporter's issue about the report has been resolved while an open report says otherwise
					</p>

					<p class="question">Q: How do i declare a report open or closed?</p>
					<p class="answer"><em>A:</em> login to your account and there's an available <strong>lock icon</strong> on each report you have made, you can use this icon to toggle between opening and closing your report.
					</p>

		        </div>
	    </div>    
	</div>

@endsection

@section('footer')
	@parent
@endsection