@extends('layouts.master')

@section('title', 'Privacy | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="privacy">
		<h1>Privacy & Cookies Policy</h1>
		<div class="col-sm-2"></div>
		<div class="col-sm-8">

		    <section>
			 <h4>1. Introduction</h4>
                <ul>
				    <li>1.1	We are committed to safeguarding the privacy of our website visitors; in this policy we explain how we will treat your personal information.</li>
					<li>1.2	By using our website and agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy.</li>
					<li>1.3	Credit: SEQ Legal (http://www.seqlegal.com).</li>
				</ul>
			</section>

		    <section>
			 <h4>2.	Collecting personal information</h4>
                <ul>
				    <li>2.1	We may collect, store and use the following kinds of personal information: <br>
					&nbsp; &nbsp; &nbsp; (a)	information about your computer and about your visits to and use of this website (including your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website navigation paths);<br>
					&nbsp; &nbsp; &nbsp; (b)	information that you provide to us when registering with our website (including  your name, email and password);<br>
					&nbsp; &nbsp; &nbsp; (c)	information that you provide when creating or editing a report on our website (including your name, phone number, email, item description, person description gender and other information depending on the kind of report);<br>
					&nbsp; &nbsp; &nbsp; (d)	information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters (including your name and email address);<br>
					&nbsp; &nbsp; &nbsp; (e)	information that you provide to us when using the services on our website, or that is generated in the course of the use of those services (including the timing, frequency and pattern of service use);<br>
					&nbsp; &nbsp; &nbsp; (f)	information contained in or relating to any communication that you send to us or send through our website (including the communication content and metadata associated with the communication); and<br>
					&nbsp; &nbsp; &nbsp; (g)	any other personal information that you choose to send to us.
					</li>
					<li>2.2	Before you disclose to us the personal information of another person, you must obtain that person's consent to both the disclosure and the processing of that personal information in accordance with this policy.</li>
				</ul>
			</section>

		    <section>
			 <h4>3.	Using personal information</h4>
                <ul>
				    <li>3.1	Personal information submitted to us through our website will be used for the purposes specified in this policy or on the relevant pages of the website.</li>
					<li>3.2	We may use your personal information to: <br>
					&nbsp; &nbsp; &nbsp; (a)	administer our website and business;<br>
					&nbsp; &nbsp; &nbsp; (b)	personalize our website for you;<br>
					&nbsp; &nbsp; &nbsp; (c)	enable your use of the services available on our website;<br>
					&nbsp; &nbsp; &nbsp; (d)	send you notifications about your reports;<br>
					&nbsp; &nbsp; &nbsp; (e)	show other users/visitors of the site details about your report;<br>
					&nbsp; &nbsp; &nbsp; (f)	provide third parties with statistical information about our users (but those third parties will not be able to identify any individual user from that information;<br>
					&nbsp; &nbsp; &nbsp; (g)	deal with enquiries and complaints made by or about you relating to our website;<br>
					&nbsp; &nbsp; &nbsp; (h)	keep our website secure and prevent fraud; and<br>
					&nbsp; &nbsp; &nbsp; (i)	verify compliance with the terms and conditions governing the use of our website.
					</li>
				</ul>
			</section>

		    <section>
			 <h4>4.	Disclosing personal information</h4>
                <ul>
				    <li>4.1	We may disclose your personal information to any of our law enforcers, insurers, professional advisers, agents, suppliers or subcontractors  insofar as reasonably necessary for the purposes set out in this policy.</li>
					<li>4.2	We may disclose your personal information to any member of our group of companies (this means our subsidiaries, our ultimate holding company and all its subsidiaries) insofar as reasonably necessary for the purposes set out in this policy.</li>
					<li>4.3	We may disclose your personal information: <br>
					&nbsp; &nbsp; &nbsp; (a)	to the extent that we are required to do so by law;<br>
					&nbsp; &nbsp; &nbsp; (b)	in connection with any ongoing or prospective legal proceedings;<br>
					&nbsp; &nbsp; &nbsp; (c)	in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk);<br>
					&nbsp; &nbsp; &nbsp; (d)	to any person who we reasonably believe may apply to a court or other competent authority for disclosure of that personal information where, in our reasonable opinion, such court or authority would be reasonably likely to order disclosure of that personal information.
					</li>
					<li>4.4	Except as provided in this policy, we will not provide your personal information to third parties.</li>
				</ul>
			</section>

		    <section>
			 <h4>5.	International data transfers</h4>
                <ul>
				    <li>5.1	Information that we collect may be stored and processed in and transferred between any of the countries in which we operate in order to enable us to use the information in accordance with this policy.</li>
					<li>5.2	Personal information that you publish on our website or submit for publication on our website may be available, via the internet, around the world. We cannot prevent the use or misuse of such information by others.</li>
					<li>5.3	You expressly agree to the transfers of personal information described in this Section 5.</li>
				</ul>
			</section>

		    <section>
			 <h4>6.	Retaining personal information</h4>
                <ul>
				    <li>6.1	This Section 6 sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations in relation to the retention and deletion of personal information.</li>
					<li>6.2	Personal information that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.</li>
					<li>6.3	Notwithstanding the other provisions of this Section 6, we will retain documents (including electronic documents) containing personal data: <br>
					&nbsp; &nbsp; &nbsp; (a)	to the extent that we are required to do so by law;<br>
					&nbsp; &nbsp; &nbsp; (b)	if we believe that the documents may be relevant to any ongoing or prospective legal proceedings; and<br>
					&nbsp; &nbsp; &nbsp; (c)	in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk).
					</li>
				</ul>
			</section>

		    <section>
			 <h4>7.	Security of personal information</h4>
                <ul>
				    <li>7.1	We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information.</li>
					<li>7.2	We will store all the personal information you provide on our secure (password- and firewall-protected) servers.</li>
					<li>7.3	All electronic financial transactions entered into through our website will be protected by encryption technology.</li>
					<li>7.4	You acknowledge that the transmission of information over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.</li>
					<li>7.5	You are responsible for keeping the password you use for accessing our website confidential; we will not ask you for your password (except when you log in to our website).</li>
				</ul>
			</section>

		    <section>
			 <h4>8.	Amendments</h4>
                <ul>
				    <li>8.1	We may update this policy from time to time by publishing a new version on our website.</li>
					<li>8.2	You should check this page occasionally to ensure you are happy with any changes to this policy.</li>
					<li>8.3	We may notify you of changes to this policy by email</li>
				</ul>
			</section>

		    <section>
			 <h4>9.	Your rights</h4>
                <ul>
				    <li>9.1	We may withhold personal information that you request to the extent permitted by law.</li>
					<li>9.2	In practice, you will usually either expressly agree in advance to our use of your personal information for marketing purposes, or we will provide you with an opportunity to opt out of the use of your personal information for marketing purposes.</li>
				</ul>
			</section>

		    <section>
			 <h4>10.	Third party websites</h4>
                <ul>
				    <li>10.1	Our website includes hyperlinks to, and details of, third party websites.</li>
					<li>10.2	We have no control over, and are not responsible for, the privacy policies and practices of third parties.</li>
				</ul>
			</section>

		    <section>
			 <h4>11.	Updating information</h4>
                <ul>
				    <li>11.1	Please let us know if the personal information that we hold about you needs to be corrected or updated.</li>
				</ul>
			</section>

		    <section>
			 <h4>12.	Cookies</h4>
                <ul>
				    <li>12.1	Our website uses cookies.</li>
					<li>12.2	A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.</li>
					<li>12.3	Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed.</li>
					<li>12.4	Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.</li>
					<li>12.5	We use both session and persistent cookies on our website.</li>
					<li>12.6	The names of the cookies that we use on our website, and the purposes for which they are used, are set out below:<br>
					&nbsp; &nbsp; &nbsp; (a)	we use persistent and session cookies to prevent fraud and improve the security of the website
					</li>
					<li>12.7	Most browsers allow you to refuse to accept cookies; for example: <br>
					&nbsp; &nbsp; &nbsp; (a)	in Internet Explorer (version 11) you can block cookies using the cookie handling override settings available by clicking "Tools", "Internet Options", "Privacy" and then "Advanced";<br>
					&nbsp; &nbsp; &nbsp; (b)	in Firefox (version 47) you can block all cookies by clicking "Tools", "Options", "Privacy", selecting "Use custom settings for history" from the drop-down menu, and unticking "Accept cookies from sites"; and<br>
					&nbsp; &nbsp; &nbsp; (c)	in Chrome (version 52), you can block all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and "Content settings", and then selecting "Block sites from setting any data" under the "Cookies" heading.
					</li>
					<li>12.8	Blocking all cookies will have a negative impact upon the usability of many websites.</li>
					<li>12.9	If you block cookies, you will not be able to use all the features on our website.</li>
					<li>12.10	You can delete cookies already stored on your computer; for example:<br>
					&nbsp; &nbsp; &nbsp; (a)	in Internet Explorer (version 11), you must manually delete cookie files (you can find instructions for doing so at http://windows.microsoft.com/en-gb/internet-explorer/delete-manage-cookies#ie=ie-11);<br>
					&nbsp; &nbsp; &nbsp; (b)	in Firefox (version 47), you can delete cookies by clicking "Tools", "Options" and "Privacy", then selecting "Use custom settings for history" from the drop-down menu, clicking "Show Cookies", and then clicking "Remove All Cookies"; and<br>
					&nbsp; &nbsp; &nbsp; (c)	in Chrome (version 52), you can delete all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and "Clear browsing data", and then selecting "Cookies and other site and plug-in data" before clicking "Clear browsing data".
					</li>
					<li>12.11	Deleting cookies will have a negative impact on the usability of many websites.</li>
				</ul>
			</section>

		    <section>
			 <h4>13.	Our details</h4>
                <ul>
				    <li>13.1	This website is owned and operated by Trakeet Team</li>
					<li>13.2	You can contact us:<br>
					&nbsp; &nbsp; &nbsp; (a)	using our website contact form;<br>
					&nbsp; &nbsp; &nbsp; (c)	by telephone, on the contact number published on our website from time to time; or<br>
					&nbsp; &nbsp; &nbsp; (d)	by email, using the email address published on our website from time to time.
					</li>
					<li>1.3	Credit: SEQ Legal (http://www.seqlegal.com).</li>
				</ul>
			</section>
        </div>
    </div>
	
	

@endsection

@section('footer')
	@parent
@endsection