@extends('layouts.master')

@section('title', 'Guide | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="how-it-works">
		<div class="col-md-12 col-sm-12 col-xs-12 segment">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1>Create A report</h1>
				<div class="col-md-2"></div>
				<div class="col-md-8 description">
					Inform people about what you've lost or found. You can only be notified on a report you have made earlier. And the best part? You do not need to register or login before creating any report
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 segment odd">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1> pictures</h1>
				<div class="col-md-2"></div>
				<div class="col-md-8 description">
					<i class="fa fa-file-picture-o"></i>
					<p>
						Adding photos to a report is an essential step to getting positive results, this feature is optional and can be achieved when logged in.  
						<h5><i class="fa fa-lightbulb-o"></i> : you need to register and log in with the same email used when creating the report </h5> 
					</p>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 segment">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1>Share on Socials</h1>
				<div class="col-md-12 social-buttons">
					<i class="fa fa-facebook-square"></i>
					<i class="fa fa-twitter-square"></i>
					<i class="fa fa-google-plus-square"></i>
					<i class="fa fa-whatsapp"></i>
				</div>
				<div class="col-md-2"></div>
				<div class="col-md-8 description">
					You can share found/missing item or missing person report with contacts on facebook, twitter, google plus and whatsapp.
					this can be achieved by clicking the icon <img src="/img/share_icon.jpg">
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12 welcome segment">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<h1>Connect your reports!</h1>
				<div class="description connect_description">
					If you've found an item that was reported missing by another user, you can pin a report to it via the icon <img src="/img/pin_report_2.jpg">. <hr>
					And if the situation is reversed i.e. you are the owner of an item that was reportedly found by another user, pin a report to it via this icon <img src="/img/pin_report_1.jpg"> <hr>
					<i class="fa fa-lightbulb-o"></i> : you can view all pinned items to a report via this icon <img src="/img/pinned_reports_icon.jpg">
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<h2 class="col-md-5 col-sm-5 col-xs-5 lost">missing
					<img class="img img-responsive img-rounded" src="/img/lost_sample.png" />
				</h2>
				<h2 class="col-md-2 col-sm-2 col-xs-2 connect_to"><i class="fa fa-map-signs"></i></h2>
				<h2 class="col-md-5 col-sm-5 col-xs-5 found">found
					<img class="img img-responsive img-rounded" src="/img/found_sample.png" />
				</h2>
				
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 segment">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1>Retrieving Your Item </h1>
				<div class="col-md-2"></div>
				<div class="col-md-8 description">
					If a missing item of yours is pinned by another person (i.e. reported found), you will be notified via email, when you log in, and via text message. 
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 welcome segment">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<h1> Status: closed/Open</h1>
				<div class="col-md-12 description connect_description">
					<p>
						Every report is declared open until closed by the owner. 
						closing a report is done when you have retrieved your missing item, or given an item you had found to its owner. <br>
						the <img src="/img/close_icon.jpg"> icon helps achieve this feature.  
					</p>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 segment odd">
				<h1> Important Notice</h1>
				<div class="col-md-12 description">
					<p>
						Trakeet is not responsible for whoever you agreed to meet at any location whether to collect your lost item or give found ones ( as stated on Section 11.7 in our <em><a href="/terms">Terms & Conditions</a></em> ).  
					</p>
				</div>
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection