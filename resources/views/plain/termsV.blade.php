@extends('layouts.master')

@section('title', 'Statistics | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	
	<div class="terms_and_conditions">
		<h1>Terms and conditions of use</h1>
		<div class="col-sm-2"></div>
		<div class="col-sm-8">

			<section>
				<h4>Introduction</h4>
				<ul>
					<li>1.1 &nbsp;These terms and conditions shall govern your use of our website.</li>
					<li>1.2 &nbsp;By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these terms and conditions, you must not use our website.</li>
					<li>1.3 &nbsp;If you register with our website, submit any report to our website or use any of our website services, we will ask you to expressly agree to these terms and conditions.</li>
					<li>1.4 &nbsp;Our website uses cookies; by using our website or agreeing to these terms and conditions, you consent to our use of cookies.</li>
					<li>1.5 &nbsp;Credit: SEQ Legal (http://www.seqlegal.com).</li>
				</ul>
			</section>

			<section>
				<h4>Copyright notice</h4>
				<ul>
					<li>2.1 &nbsp; Copyright (c) 2017 Trakeet</li>
					<li>2.2 &nbsp; Subject to the express provisions of these terms and conditions:<br>
					&nbsp; &nbsp; &nbsp; (a)	we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and<br>
					&nbsp; &nbsp; &nbsp; (b)	all the copyright and other intellectual property rights in our website and the material on our website are reserved.
					</li>
				</ul>
			</section>

			<section>
				<h4>Licence to use website</h4>
				<ul>
					<li>3.1 &nbsp; You may:<br>
					&nbsp; &nbsp; &nbsp; (a)	view pages from our website in a web browser;<br>
					&nbsp; &nbsp; &nbsp; (b)	download pages from our website for caching in a web browser;<br>
					&nbsp; &nbsp; &nbsp; (c)	print pages from our website;<br>
					&nbsp; &nbsp; &nbsp; (d)	report or check missing items, found items and missing people<br>
					&nbsp; &nbsp; &nbsp; (e)	perform authorized actions on reports made by you.<br>
						subject to the other provisions of these terms and conditions.
					</li>
					<li>3.12 &nbsp; You may only use our website for your own personal purposes, and you must not use our website for any other purposes.</li>
					<li>3.3 &nbsp; Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on our website.</li>
					<li>3.4 &nbsp; Unless you own or control the relevant rights on reports, you must not:<br>
					&nbsp; &nbsp; &nbsp; (a)	attempt to edit or modify any report<br>
					&nbsp; &nbsp; &nbsp; (b)	exploit reports from our website for a commercial purpose.
					</li>
					<li>3.5 &nbsp; Notwithstanding Section 4.5, you may share reports on social networks</li>
					<li>3.6 &nbsp; We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.</li>
				</ul>
			</section>

			<section>
				<h4>Acceptable use</h4>
				<ul>
					<li>4.1 &nbsp; You must not:<br>
					&nbsp; &nbsp; &nbsp; (a)	use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;<br>
					&nbsp; &nbsp; &nbsp; (b)	use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;<br>
					&nbsp; &nbsp; &nbsp; (c)	use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;<br>
					&nbsp; &nbsp; &nbsp; (d)	conduct any systematic or automated data collection activities including without limitation scraping, data mining, data extraction and data harvesting on or in relation to our website without our express written consent;<br>
					&nbsp; &nbsp; &nbsp; (e)	access or otherwise interact with our website using any robot, spider or other automated means, except for the purpose of search engine indexing<br>
					&nbsp; &nbsp; &nbsp; (f)	violate the directives set out in the robots.txt file for our website or<br>
					&nbsp; &nbsp; &nbsp; (g)	use data collected from our website for any direct marketing activity including without limitation email marketing, SMS marketing, telemarketing and direct mailing.
					</li>
					<li>4.2 &nbsp; You must ensure that all the information you supply to us through our website, or in relation to our website, is true, accurate, current, complete and non-misleading.</li>
				</ul>
			</section>

			<section>
				<h4>Registration and accounts</h4>
				<ul>
					<li>5.1	&nbsp; To be eligible for an account on our website under this Section 6, you must be resident or situated in Nigeria.</li>
					<li>5.2	&nbsp; You may register for an account with our website by completing and submitting the account registration form on our website.</li>
					<li>5.3	&nbsp; You must not allow any other person to use your account to access the website.</li>
					<li>5.4	&nbsp; You must notify us in writing immediately if you become aware of any unauthorised use of your account.</li>
					<li>5.5	&nbsp; You must not use any other person's account to access the website unless you have that person's express permission to do so.</li>
				</ul>
			</section>

			<section>
				<h4>User login details</h4>
				<ul>
					<li>6.1 &nbsp;	If you register for an account with our website, you will be asked to submit your name, email, and choose a password.</li>
					<li>6.2 &nbsp;	you must not use your email or account for or in connection with the impersonation of any person. </li>
					<li>6.3 &nbsp;	You must keep your password confidential.</li>
					<li>6.4 &nbsp;	You must notify us in writing immediately if you become aware of any disclosure of your password.</li>
					<li>6.5 &nbsp;	You are responsible for any activity on our website arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure.</li>
				</ul>
			</section>

			<section>
				<h4>Cancellation of a report and suspension of account</h4>
				<ul>
					<li>7.1 &nbsp;	We may:
					(a)	edit or delete any report <br>
					(b)	cancel your account and/or<br>
					(c)	edit your account details,<br>
						at any time in our sole discretion without notice or explanation.
					</li>
					<li>7.2 &nbsp;	You may delete your reports and cancel your account on our website.</li>
				</ul>
			</section>

			<section>
				<h4>Your content: license</h4>
				<ul>
					<li>8.1	&nbsp; In these terms and conditions, "your content" means all info, reports, works and materials (including without limitation text, graphics, images, audio material, video material, audio-visual material, scripts, software and files) that you submit to us or our website for storage or publication on, processing by, or transmission via, our website.</li>
					<li>8.2	&nbsp; You grant to us a worldwide, irrevocable, non-exclusive, royalty-free licence to use, reproduce, store, adapt, publish, translate and distribute your content in any existing or future media.</li>
					<li>8.3	&nbsp; You grant to us the right to sub-license the rights licensed under Section 8.2.</li>
					<li>8.4	&nbsp; You grant to us the right to bring an action for infringement of the rights licensed under Section 8.2.</li>
					<li>8.5	&nbsp; You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other moral rights in your content have been waived to the maximum extent permitted by applicable law.</li>
					<li>8.6	&nbsp; You may edit your content to the extent permitted using the editing functionality made available on our website.</li>
					<li>8.7	&nbsp; Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may delete, unpublish or edit any or all of your content.</li>
				</ul>
			</section>

			<section>
				<h4>Your content: rules</h4>
				<ul>
					<li>9.1 &nbsp;	You warrant and represent that your content will comply with these terms and conditions.</li>
					<li>9.2 &nbsp;	Your content must not be illegal or unlawful, must not infringe any person's legal rights, and must not be capable of giving rise to legal action against any person (in each case in any jurisdiction and under any applicable law).</li>
					<li>9.3 &nbsp;	Your content, and the use of your content by us in accordance with these terms and conditions, must not:
					&nbsp; &nbsp; &nbsp; (a)	be libellous or maliciously false;<br>
					&nbsp; &nbsp; &nbsp; (b)	be obscene or indecent;<br>
					&nbsp; &nbsp; &nbsp; (c)	infringe any copyright, moral right, database right, trade mark right, design right, right in passing off, or other intellectual property right;<br>
					&nbsp; &nbsp; &nbsp; (d)	infringe any right of confidence, right of privacy or right under data protection legislation;<br>
					&nbsp; &nbsp; &nbsp; (e)	constitute negligent advice or contain any negligent statement;<br>
					&nbsp; &nbsp; &nbsp; (f)	constitute an incitement to commit a crime, instructions for the commission of a crime or the promotion of criminal activity;<br>
					&nbsp; &nbsp; &nbsp; (g)	be in contempt of any court, or in breach of any court order;<br>
					&nbsp; &nbsp; &nbsp; (h)	be in breach of racial or religious hatred or discrimination legislation;<br>
					&nbsp; &nbsp; &nbsp; (i)	be blasphemous;<br>
					&nbsp; &nbsp; &nbsp; (j)	be in breach of official secrets legislation;<br>
					&nbsp; &nbsp; &nbsp; (k)	be in breach of any contractual obligation owed to any person;<br>
					&nbsp; &nbsp; &nbsp; (l)	depict violence in an explicit, graphic or gratuitous manner;<br>
					&nbsp; &nbsp; &nbsp; (m)	be pornographic, lewd, suggestive or sexually explicit;<br>
					&nbsp; &nbsp; &nbsp; (n)	be untrue, false, inaccurate or misleading;<br>
					&nbsp; &nbsp; &nbsp; (o)	consist of or contain any instructions, advice or other information which may be acted upon and could, if acted upon, cause illness, injury or death, or any other loss or damage;<br>
					&nbsp; &nbsp; &nbsp; (p)	constitute spam;<br>
					&nbsp; &nbsp; &nbsp; (q)	be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory; <br>or
					&nbsp; &nbsp; &nbsp; (r)	cause annoyance, inconvenience or needless anxiety to any person.
					</li>
				</ul>
			</section>

			<section>
				<h4>Limited warranties</h4>
				<ul>
					<li>10.1	We do not warrant or represent:
					&nbsp; &nbsp; &nbsp; (a)	the completeness or accuracy of the information published on our website;<br>
					&nbsp; &nbsp; &nbsp; (b)	that the material on the website is up to date;<br> or
					&nbsp; &nbsp; &nbsp; (c)	that the website or any service on the website will remain available.
					</li>
					<li>10.2	We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent expressly provided otherwise in these terms and conditions, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.</li>
					<li>10.3	To the maximum extent permitted by applicable law and subject to Section 11.1, we exclude all representations and warranties relating to the subject matter of these terms and conditions, our website and the use of our website.</li>
				</ul>
			</section>

			<section>
				<h4>Limitations and exclusions of liability</h4>
				<ul>
					<li>11.1	Nothing in these terms and conditions will:
					&nbsp; &nbsp; &nbsp; (a)	limit or exclude any liability for death or personal injury resulting from negligence;<br>
					&nbsp; &nbsp; &nbsp; (b)	limit or exclude any liability for fraud or fraudulent misrepresentation;<br>
					&nbsp; &nbsp; &nbsp; (c)	limit any liabilities in any way that is not permitted under applicable law;<br> or
					&nbsp; &nbsp; &nbsp; (d)	exclude any liabilities that may not be excluded under applicable law.
					</li>
					<li>11.2	The limitations and exclusions of liability set out in this Section 11 and elsewhere in these terms and conditions: 
					&nbsp; &nbsp; &nbsp; (a)	are subject to Section 11.1; <br>and
					&nbsp; &nbsp; &nbsp; (b)	govern all liabilities arising under these terms and conditions or relating to the subject matter of these terms and conditions, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty, except to the extent expressly provided otherwise in these terms and conditions.
					</li>
					<li>11.3	To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.</li>
					<li>11.4	We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.</li>
					<li>11.5	We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.</li>
					<li>11.6	We will not be liable to you in respect of any loss or corruption of any data, database or software.</li>
					<li>11.7	We will not be liable to you in respect of any special, indirect or consequential harm, loss or damage resulting from whoever you meet via our website.</li>
					<li>11.8	You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).</li>
				</ul>
			</section>

			<section>
				<h4>Breaches of these terms and conditions</h4>
				<ul>
					<li>12.1	Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may: 
					&nbsp; &nbsp; &nbsp; (a)	send you one or more formal warnings;<br>
					&nbsp; &nbsp; &nbsp; (b)	temporarily suspend your access to our website;<br>
					&nbsp; &nbsp; &nbsp; (c)	permanently prohibit you from accessing our website;<br>
					&nbsp; &nbsp; &nbsp; (d)	block computers using your IP address from accessing our website;<br>
					&nbsp; &nbsp; &nbsp; (e)	contact any or all of your internet service providers and request that they block your access to our website;<br>
					&nbsp; &nbsp; &nbsp; (f)	commence legal action against you, whether for breach of contract or otherwise; <br>and/or
					&nbsp; &nbsp; &nbsp; (g)	suspend or delete your account on our website.
					</li>
					<li>12.2	Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).</li>
				</ul>
			</section>

			<section>
				<h4>Variation</h4>
				<ul>
					<li>13.1	We may revise these terms and conditions from time to time.</li>
					<li>13.2	The revised terms and conditions shall apply to the use of our website from the date of publication of the revised terms and conditions on the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of these terms and conditions</li>
					<li>13.3	If you have given your express agreement to these terms and conditions, we will ask for your express agreement to any revision of these terms and conditions; and if you do not give your express agreement to the revised terms and conditions within such period as we may specify, we will disable or delete your account on the website, and you must stop using the website.</li>
				</ul>
			</section>

			<section>
				<h4>Assignment</h4>
				<ul>
					<li>14.1	You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and conditions. </li>
					<li>14.2	You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these terms and conditions. </li>
				</ul>
			</section>

			<section>
				<h4>Severability</h4>
				<ul>
					<li>15.1	If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.</li>
					<li>15.2	If any unlawful and/or unenforceable provision of these terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect. </li>
				</ul>
			</section>

			<section>
				<h4>Third party rights</h4>
				<ul>
					<li>16.1	A contract under these terms and conditions is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.</li>
					<li>16.2	The exercise of the parties' rights under a contract under these terms and conditions is not subject to the consent of any third party.</li>
				</ul>
			</section>

			<section>
				<h4>Entire agreement</h4>
				<ul>
					<li>17.1	Subject to Section 12.1, these terms and conditions shall constitute the entire agreement between you and us in relation to your use of our website and shall supersede all previous agreements between you and us in relation to your use of our website.</li>
				</ul>
			</section>

			<section>
				<h4>Law and jurisdiction</h4>
				<ul>
					<li>18.1	These terms and conditions shall be governed by and construed in accordance with Nigerian Law</li>
					<li>18.2	Any disputes relating to these terms and conditions shall be subject to the non-exclusive jurisdiction of the courts of Nigeria.</li>
				</ul>
			</section>

			<section>
				<h4>Registration</h4>
				<ul>
					<li>20.1	This website is owned and operated by Trakeet.</li>
					<li>20.2	You can contact us:
					&nbsp; &nbsp; &nbsp; (a)	using our website contact form;<br>
					&nbsp; &nbsp; &nbsp; (b)	by telephone, on the contact number published on our website from time to time;<br> or
					&nbsp; &nbsp; &nbsp; (c)	by email, using the email address published on our website from time to time.
					</li>
				</ul>
			</section>

		</div>
		<div class="col-sm-2"></div>
	</div>

@endsection

@section('footer')
	@parent
@endsection