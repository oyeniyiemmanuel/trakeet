@extends('layouts.master')

@section('title', 'Trakeet | Founders')

@section('side_navbar')

@endsection

@section('content')
	<div class="founders">
	<!--
	  -- $trakeet_founders is a global array shared with all views and is
	  -- registered in app/Providers/AppServiceProvider.php
	  -->
		@if(!empty($trakeet_founders))
			 <ul>
				<h4>FOUNDERS</h4>
			   @foreach ($trakeet_founders as $founder)
					[ <li> {{$founder}} </li> ]
			   @endforeach
			</ul>
		@endif
		<hr />
		@if(empty($others))
			<ul>
				<h4>OTHER PARTICIPANTS</h4>
				<p> There are no other participants</p>
			</ul>
		@else
			<ul>
				<h4>OTHER PARTICIPANTS</h4>
			   @foreach ($others as $other)
					<li>{{$other}}</li>
			   @endforeach
			</ul>
		@endif
	</div>
@endsection

@section('footer')
	@parent
@endsection
		

