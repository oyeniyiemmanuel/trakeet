@extends('layouts.master')

@section('title', 'Disclaimer | Trakeet')

@section('side_navbar')
	@parent
@endsection

@section('content')
	<div class="disclaimer">
		<h1>Disclaimer</h1>
		<hr>
		<div class="col-sm-2"></div>
		<div class="col-sm-8">

		    <section>
			 <h4>Introduction</h4>
                <ul>
				    <li>1.1 &nbsp;This disclaimer shall govern your use of our website.</li>
					<li>1.2 &nbsp;By using our website, you accept this disclaimer in full; accordingly, if you disagree with this disclaimer or any part of this disclaimer, you must not use our website.</li>
					<li>1.3 &nbsp;If you register with our website, submit any report to our website or use any of our website services, we will ask you to expressly agree to these disclaimer.</li>
					<li>1.4 &nbsp;Our website uses cookies; by using our website or agreeing to this disclaimer, you consent to our use of cookies.</li>
					<li>1.5 &nbsp;Credit: SEQ Legal (http://www.seqlegal.com).</li>
				</ul>
			</section>

			<section>
				<h4>Copyright notice</h4>
				<ul>
					<li>2.1 &nbsp; Copyright (c) 2017 Trakeet</li>
					<li>2.2 &nbsp; Subject to the express provisions of this disclaimer:<br>
					&nbsp; &nbsp; &nbsp; (a)	we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and<br>
					&nbsp; &nbsp; &nbsp; (b)	all the copyright and other intellectual property rights in our website and the material on our website are reserved.
					</li>
				</ul>
			</section>

			<section>
				<h4>Licence to use website</h4>
				<ul>
					<li>3.1 &nbsp; You may:<br>
					&nbsp; &nbsp; &nbsp; (a)	view pages from our website in a web browser;<br>
					&nbsp; &nbsp; &nbsp; (b)	download pages from our website for caching in a web browser;<br>
					&nbsp; &nbsp; &nbsp; (c)	print pages from our website;<br>
					&nbsp; &nbsp; &nbsp; (d)	report or check missing items, found items and missing people<br>
					&nbsp; &nbsp; &nbsp; (e)	perform authorized actions on reports made by you.<br>
						subject to the other provisions of this disclaimer.
					</li>
					<li>3.12 &nbsp; You may only use our website for your own personal purposes, and you must not use our website for any other purposes.</li>
					<li>3.3 &nbsp; Except as expressly permitted by this disclaimer, you must not edit or otherwise modify any material on our website.</li>
					<li>3.4 &nbsp; Unless you own or control the relevant rights on reports, you must not:<br>
					&nbsp; &nbsp; &nbsp; (a)	attempt to edit or modify any report<br>
					&nbsp; &nbsp; &nbsp; (b)	exploit reports from our website for a commercial purpose.
					&nbsp; &nbsp; &nbsp; (c)    redistribute material from our website
					</li>
					<li>3.5 &nbsp; Notwithstanding Section 4.5, you may share reports on social networks</li>
					<li>3.6 &nbsp; We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.</li>
				</ul>
			</section>

			<section>
				<h4>Acceptable use</h4>
				<ul>
					<li>4.1 &nbsp; You must not:<br>
					&nbsp; &nbsp; &nbsp; (a)	use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;<br>
					&nbsp; &nbsp; &nbsp; (b)	use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;<br>
					&nbsp; &nbsp; &nbsp; (c)	use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;<br>
					&nbsp; &nbsp; &nbsp; (d)	conduct any systematic or automated data collection activities including without limitation scraping, data mining, data extraction and data harvesting on or in relation to our website without our express written consent;<br>
					&nbsp; &nbsp; &nbsp; (e)	access or otherwise interact with our website using any robot, spider or other automated means, except for the purpose of search engine indexing<br>
					&nbsp; &nbsp; &nbsp; (f)	violate the directives set out in the robots.txt file for our website or<br>
					&nbsp; &nbsp; &nbsp; (g)	use data collected from our website for any direct marketing activity including without limitation email marketing, SMS marketing, telemarketing and direct mailing.
					</li>
					<li>4.2 &nbsp; You must ensure that all the information you supply to us through our website, or in relation to our website, is true, accurate, current, complete and non-misleading.</li>
				</ul>
			</section>
			<section>
				<h4>Limited warranties</h4>
				<ul>
					<li>5.1	We do not warrant or represent:
					&nbsp; &nbsp; &nbsp; (a)	the completeness or accuracy of the information published on our website;<br>
					&nbsp; &nbsp; &nbsp; (b)	that the material on the website is up to date;<br> or
					&nbsp; &nbsp; &nbsp; (c)	that the website or any service on the website will remain available.
					</li>
					<li>5.2	We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent expressly provided otherwise in this disclaimer, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.</li>
					<li>5.3	To the maximum extent permitted by applicable law and subject to Section 6.1, we exclude all representations and warranties relating to the subject matter of this disclaimer, our website and the use of our website.</li>
				</ul>
			</section>

			<section>
				<h4>Limitations and exclusions of liability</h4>
				<ul>
					<li>6.1	Nothing in this disclaimer will:
					&nbsp; &nbsp; &nbsp; (a)	limit or exclude any liability for death or personal injury resulting from negligence;<br>
					&nbsp; &nbsp; &nbsp; (b)	limit or exclude any liability for fraud or fraudulent misrepresentation;<br>
					&nbsp; &nbsp; &nbsp; (c)	limit any liabilities in any way that is not permitted under applicable law;<br> or
					&nbsp; &nbsp; &nbsp; (d)	exclude any liabilities that may not be excluded under applicable law.
					</li>
					<li>6.2	The limitations and exclusions of liability set out in this Section 6 and elsewhere in this disclaimer: 
					&nbsp; &nbsp; &nbsp; (a)	are subject to Section 6.1; <br>and
					&nbsp; &nbsp; &nbsp; (b)	govern all liabilities arising under this disclaimer or relating to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty, except to the extent expressly provided otherwise in this disclaimer.
					</li>
					<li>6.3	To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.</li>
					<li>6.4	We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.</li>
					<li>6.5	We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.</li>
					<li>6.6	We will not be liable to you in respect of any loss or corruption of any data, database or software.</li>
					<li>6.7	We will not be liable to you in respect of any special, indirect or consequential harm, loss or damage resulting from whoever you meet via our website.</li>
					<li>6.8	You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or this disclaimer (this will not, of course, limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).</li>
				</ul>
			</section>
			<section>
				<h4>Variation</h4>
				<ul>
					<li>7.1	We may revise this disclaimer from time to time.</li>
					<li>7.2	The revised disclaimer shall apply to the use of our website from the date of publication of the revised disclaimer on the website.
					</li>
				</ul>
			</section>
			<section>
				<h4>Severability</h4>
				<ul>
					<li>8.1	If a provision of this disclaimer is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.</li>
					<li>8.2	If any unlawful and/or unenforceable provision of this disclaimer would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect. </li>
				</ul>
			</section>
			<section>
				<h4>Law and jurisdiction</h4>
				<ul>
					<li>9.1	This disclaimer shall be governed by and construed in accordance with Nigerian Law</li>
					<li>9.2	Any disputes relating to this disclaimer shall be subject to the non-exclusive jurisdiction of the courts of Nigeria.</li>
				</ul>
			</section>
			<section>
				<h4>Our details</h4>
				<ul>
					<li>10.1	This website is owned and operated by Trakeet.</li>
					<li>10.2	You can contact us:
					&nbsp; &nbsp; &nbsp; (a)	using our website contact form;<br>
					&nbsp; &nbsp; &nbsp; (b)	by telephone, on the contact number published on our website from time to time;<br> or
					&nbsp; &nbsp; &nbsp; (c)	by email, using the email address published on our website from time to time.
					</li>
				</ul>
			</section>
			</div>
		<div class="col-sm-2"></div> 
    </div>
	
	

@endsection

@section('footer')
	@parent
@endsection