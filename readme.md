# HOW TO SETUP
I'm assuming yall have WAMP, first create a work location folder called "trakeet-project" in your C:\wamp\www folder then follow the steps below. oshee	

## PULL THE REPO
		Sourcetree is the app you will use to pull and push files, 
		but you have to connect to the gitlab repository from your
		Sourcetree app before you can start editing the repo files.
		
		a. download sourcetree for windows at https://www.atlassian.com/software/sourcetree , install and run
		b. click the "clone/New" on the app and clone the repository by putting the following details.
			Source Path/URL-> https://gitlab.com/oyeniyiemmanuel/trakeet.git
			Destination Path -> C:\wamp\www\trakeet-project 
		c. click "clone"
		d. done.
		

## LETS INSTALL COMPOSER
		Laravel is the PHP framework to use for this project and 
		it needs COMPOSER so we have to install that first
		
		a. Go to https://getcomposer.org/download/ and download the Composer-Setup.exe, run and install
		b. Click on WAMP icon->PHP->PHP Extensions and enable: php_openssl, php_curl, php_socket.
		c. Click on wamp icon->Apache->Apache Modules and enable ssl_module
		d. To check if composer installed properly, Open your command prompt and type "composer",
			it will show something like this and some bunch of other jargons.
			   ______
			  / ____/___  ____ ___  ____  ____  ________  _____
			 / /   / __ \/ __ `__ \/ __ \/ __ \/ ___/ _ \/ ___/
			/ /___/ /_/ / / / / / / /_/ / /_/ (__  )  __/ /
			\____/\____/_/ /_/ /_/ .___/\____/____/\___/_/
								/_/
		e. navigate to your work folder in your cmd i.e. type "cd C:\wamp\www\trakeet-project" 
		f. type "composer install" . wait and let it download all the dependencies.
		g. done

## SETUP YOUR .ENV FILE
		open your trakeet-project folder, you'll see a .env.example file
		a. rename it to .env
		b. go to your command prompt and type "php artisan key:generate"
		c. done
			
## LETS SETUP VHOST (Virtual Host - so we can access our app on 'trakeet.dev' and not 'localhost/warefa').

	PHASE 1
		a. go to C:\wamp\bin\apache\apache2.4.9\conf\extra 
		b. open the "httpd-vhosts.conf" file in your editor
		c. add the following code to it
		
				<VirtualHost *:80>
					ServerAdmin oyeniyiemmanuel@gmail.com
					DocumentRoot "C:/wamp/www/trakeet-project/public"
					ServerName trakeet.dev
					<Directory "/">
						Allow from all
						Allow from 127.0.0.1
					</Directory>
				</VirtualHost>
				
		d. done.
	
	PHASE 2
		a. now go to "C:\Windows\System32\drivers\etc"
		b. open the "hosts" file in your editor
		c. add the following code to the next line after "127.0.0.1		localhost"
			
			127.0.0.1		trakeet.dev
			
		d. restart all WAMP services.
		e. go to trakeet.dev on your browser
		
		DAZALL - After this, our apache will be listening to trakeet.dev
			
## Contributing team
    Graphics Designer(GAMEKYD) 
    Front-end Developer(NAKEL & NUEL) 
    Back-end Developer(NUEL) 
    Project Manager(GAMEKYD, NAKEL, NUEL)
