<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisabilityMissingPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disability_missing_person', function (Blueprint $table){
            $table->integer('missing_person_id')->unsigned()->index()->nullable();
            $table->foreign('missing_person_id')->references('id')->on('missing_people')->onDelete('cascade');
            
            $table->integer('disability_id')->unsigned()->index()->nullable();
            $table->foreign('disability_id')->references('id')->on('disabilities')->onDelete('cascade');

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('disability_missing_person');
    }
}
