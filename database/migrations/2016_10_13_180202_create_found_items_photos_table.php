<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoundItemsPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('found_item_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('found_item_id')->unsigned()->nullable();
            $table->foreign('found_item_id')->references('id')->on('found_items')->onDelete('cascade');
            $table->string('thumbnail_path')->nullable();
            $table->string('path')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('found_item_photos');
    }
}
