<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguageMissingPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_missing_person', function (Blueprint $table){
            $table->integer('missing_person_id')->unsigned()->index()->nullable();
            $table->foreign('missing_person_id')->references('id')->on('missing_people')->onDelete('cascade');
            
            $table->integer('language_id')->unsigned()->index()->nullable();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('language_missing_person');
    }
}
