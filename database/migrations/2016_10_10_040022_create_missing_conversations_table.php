<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissingConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missing_conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('missing_comment_id')->index()->unsigned()->nullable();
            $table->foreign('missing_comment_id')->references('id')->on('missing_comments')->onDelete('cascade');

            $table->integer('missing_item_id')->index()->unsigned()->nullable();
            $table->foreign('missing_item_id')->references('id')->on('missing_items')->onDelete('cascade');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('missing_conversations');
    }
}
