<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistinctFeatureMissingPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distinct_feature_missing_person', function (Blueprint $table){
            $table->integer('missing_person_id')->unsigned()->index()->nullable();
            $table->foreign('missing_person_id')->references('id')->on('missing_people')->onDelete('cascade');
            
            $table->integer('distinct_feature_id')->unsigned()->index()->nullable();
            $table->foreign('distinct_feature_id')->references('id')->on('distinct_features')->onDelete('cascade');

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('distinct_feature_missing_person');

    }
}