<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoundItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('found_items', function(Blueprint $table){
			$table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
			$table->string('slug')->unique()->nullable();
			$table->string('item_category')->nullable();
			$table->text('item_description')->nullable();
			$table->string('last_seen_state')->nullable();
			$table->string('last_seen_time')->nullable();
			$table->string('reporter_email')->nullable();
			$table->string('reporter_name')->nullable();
			$table->string('reporter_phone')->nullable();
            $table->boolean('closed')->default(false);
			$table->nullableTimestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('found_items');
    }
}
