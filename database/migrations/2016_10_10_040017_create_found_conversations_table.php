<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoundConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('found_conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('found_comment_id')->index()->unsigned()->nullable();
            $table->foreign('found_comment_id')->references('id')->on('found_comments')->onDelete('cascade');

            $table->integer('found_item_id')->index()->unsigned()->nullable();
            $table->foreign('found_item_id')->references('id')->on('found_items')->onDelete('cascade');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('found_conversations');
    }
}
