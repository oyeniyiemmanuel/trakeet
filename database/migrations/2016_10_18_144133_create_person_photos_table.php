<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missing_people_photo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('missing_person_id')->unsigned()->nullable();
            $table->foreign('missing_person_id')->references('id')->on('missing_people')->onDelete('cascade');
            $table->string('thumbnail_path')->nullable();
            $table->string('path')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('missing_people_photo');
    }
}
