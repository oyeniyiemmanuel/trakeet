<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissingButFoundItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missing_but_found_items', function (Blueprint $table) {
            $table->integer('missing_item_id')->unsigned()->index()->nullable();
            $table->foreign('missing_item_id')->references('id')->on('missing_items')->onDelete('cascade');
            
            $table->integer('found_item_id')->unsigned()->index()->nullable();
            $table->foreign('found_item_id')->references('id')->on('found_items')->onDelete('cascade');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('missing_but_found_items');
    }
}
