<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissingItemPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missing_item_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('missing_item_id')->unsigned()->nullable();
            $table->foreign('missing_item_id')->references('id')->on('missing_items')->onDelete('cascade');
            $table->string('thumbnail_path')->nullable();
            $table->string('path')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('missing_item_photos');
    }
}
