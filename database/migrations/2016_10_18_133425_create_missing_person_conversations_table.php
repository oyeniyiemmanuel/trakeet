<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissingPersonConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missing_person_conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_comment_id')->index()->unsigned()->nullable();
            $table->foreign('person_comment_id')->references('id')->on('person_comments')->onDelete('cascade');

            $table->integer('missing_person_id')->index()->unsigned()->nullable();
            $table->foreign('missing_person_id')->references('id')->on('missing_people')->onDelete('cascade');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('missing_person_conversations');
    }
}
