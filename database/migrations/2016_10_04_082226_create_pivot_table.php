<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('found_item_last_seen_area', function (Blueprint $table){
            $table->integer('found_item_id')->unsigned()->index()->nullable();
            $table->foreign('found_item_id')->references('id')->on('found_items')->onDelete('cascade');
            
            $table->integer('last_seen_area_id')->unsigned()->index()->nullable();
            $table->foreign('last_seen_area_id')->references('id')->on('last_seen_areas')->onDelete('cascade');

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('found_item_last_seen_area');
    }
}
