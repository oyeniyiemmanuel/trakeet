<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivot2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_seen_area_missing_item', function (Blueprint $table){
            $table->integer('missing_item_id')->unsigned()->index()->nullable();
            $table->foreign('missing_item_id')->references('id')->on('missing_items')->onDelete('cascade');
            
            $table->integer('last_seen_area_id')->unsigned()->index()->nullable();
            $table->foreign('last_seen_area_id')->references('id')->on('last_seen_areas')->onDelete('cascade');

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('last_seen_area_missing_item');
    }
}
